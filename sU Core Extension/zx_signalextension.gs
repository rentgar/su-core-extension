include "interfaceprovider.gs"
include "zx_signal.gs"
include "logic.gs"


class zxSignalExtension isclass zxSignal_main, InterfaceProviderBase
{
  Asset izojoin; //Изостык
  bool izojoinreverse; //Указатель, что изостык развёрнут
  Asset[] tmpassets;

  int devices; //Набор дополнительных навесов
  Asset signt; //Знак условно разрешающий

  define int DEV_SIGNT            = 1; //Условно разрешающий знак "Т"
  define int DEV_ARROW            = 2; //Стрелки коротко блок участка
  define int DEV_POINTER          = 4; //Маршрутный указатель
  define int DEV_SIGNTANDARROW    = 8; //Условно разрешающий и стрелки могут быть вместе
  define int DEV_SIGNTANDPOINTER  = 16; //Условно разрешающий и маршрутный указатель могут быть вместе
  define int DEV_ARROWANDPOINTER  = 32; //Стрелки и маршрутный указатель могут быть вместе



  final string GetIcon(string Name)
  {
    KUID kuid = me.GetAsset().FindAsset("extension-core").FindAsset(Name).GetKUID();
    return HTMLWindow.MakeImage(kuid.GetHTMLString(), true);
  }

  void UpdateIzojoin(void)
  {
    MeshObject mo = me.GetFXAttachment("izojoin");
    if(izojoin and (!mo or mo.GetAsset() != izojoin))
      mo = me.SetFXAttachment("izojoin", me.izojoin);
    else if(!me.izojoin) mo = me.SetFXAttachment("izojoin", null);
    if(mo and izojoinreverse)
      mo.SetMeshOrientation("default", 0.0, 0.0, Math.PI);
    else if(mo) mo.SetMeshOrientation("default", 0.0, 0.0, 0.0);
  }

  void UpdateSignalPosition(void)
  {
    me.SetMeshTranslation("default", displacement, along_displ, vert_displ);
  }


  string[] GetLensKit(void)
  {
    Soup soup = me.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("lens-kits");
    int i, count = soup.CountTags();
	  string[] ret = new string[count];
	  for(i = 0; i < count; i++){
      string lens = soup.GetNamedSoup(i).GetNamedTag("lenses");
      if(lens == ""){
        ret[i, count] = null;
        break;
      }else ret[i] = lens;
		}
	  return ret;
  }

  string TranslNames(string name)
	{
	  string ret="";
	  string main1 = STT.GetString("lens_str1");
	  string add1= STT.GetString("lens_str2");
	  int i;
	  for(i = name.size() - 1; i >= 0; i--){
		  if(name[i] == 'R') ret = "-" + main1[0,2] + ret;
		  else if(name[i] == 'G') ret = "-" + main1[2,4] + ret;
		  else if(name[i] == 'Y') ret = "-" + main1[6,8] + ret;
		  else if(name[i] == 'W') ret = "-" + main1[12,14] + ret;
		  else if(name[i] == 'B') ret = "-" + main1[16,18] + ret;
		  else if(name[i] == 'L') ret = "-" + main1[2,4] + main1[18,20] + ret;
		  else if(name[i] == 'b') ret = add1 + ret;
		  else ret = "-" + name[i,i+1] + ret;
		}
	  ret = ret[1,];
	  return ret;
	}

  void MakeBUArrow(void)
  {
    MeshObject arrow = me.GetFXAttachment("arrows");
    if(kor_BU_1 or kor_BU_2){
      if(!arrow) arrow = me.SetFXAttachment("arrows", me.GetAsset().FindAsset("arrows"));
      if(kor_BU_1) arrow.SetMesh("arrowone", 0.0);
      else arrow.SetMesh("arrowtwo", 0.0);
    }else if(arrow) me.SetFXAttachment("arrows", null);
  }

  void SetBUArrow(bool state)
	{
    MeshObject arrow = me.GetFXAttachment("arrows");
    if(arrow and kor_BU_2){
      arrow.SetMeshVisible("arrowtwo-light-top", state, 0.5);
      arrow.SetMeshVisible("arrowtwo-light-bottom", state, 0.5);
    }if(arrow and kor_BU_1) arrow.SetMeshVisible("arrowone-light", state, 0.5);
  }

  void SetNewGolPosition(bool enable)
  {
    int code = Logic.SetStat(0, DEV_SIGNT, devices & DEV_SIGNT and signt != null);
    code = Logic.SetStat(code, DEV_ARROW, devices & DEV_ARROW and (kor_BU_1 or kor_BU_2));
    code = Logic.SetStat(code, DEV_POINTER, devices & DEV_POINTER and enable);
    string sposition = me.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedTag("position-" + code);
    if(sposition != ""){
      string[] spostok = Str.Tokens(sposition,",");
      int i, count = spostok.size();
      for(i = 0; i < count; i++){
        if(i < count - 3) me.SetMeshTranslation("kreplenie" + i, 0, 0, Str.ToFloat(spostok[i]));
        else if(i == count - 3) me.SetMeshTranslation("signt", 0, 0, Str.ToFloat(spostok[i]));
        else if(i == count - 2) me.SetMeshTranslation("arrows", 0, 0, Str.ToFloat(spostok[i]));
        else me.SetMeshTranslation("pointer", 0, 0, Str.ToFloat(spostok[i]));
      }
    }
  }






  final string GetStringBool(bool stat)
  {
    if(stat) return strTable.GetString("yes");
    return strTable.GetString("no");
  }

  final string GetStringLockedType(void)
  {
    if(ab4) return STT.GetString("ab4");
    return STT.GetString("ab3");
  }

  string GetPropertyType(string propertyID)
  {
    if(propertyID == "izojoin") return "list,1";
    return inherited(propertyID);
  }

  public string[] GetPropertyElementList(string propertyID)
  {
    if(propertyID == "izojoin"){
      Asset[] sassets = TrainzScript.GetAssetList("scenary", "TSIZJ");
      Asset[] massets = TrainzScript.GetAssetList("mesh", "TSIZJ");
      //Asset[] tassets = new  Asset[0];//TrainzScript.GetAssetList("trackobjects", "TSIZJ");
      string[] ret = new string[sassets.size() + massets.size()]; // + tassets.size()
      tmpassets = new Asset[sassets.size() + massets.size()]; // + tassets.size()
      int i;
      for(i = 0; i < sassets.size() + massets.size(); i++){ // + tassets.size()
//        if(i >= sassets.size() + massets.size()){
//          ret[i] = tassets[i - sassets.size() - massets.size()].GetLocalisedName();
//          tmpassets[i] = tassets[i - sassets.size() - massets.size()];
        if(i >= sassets.size()){
          ret[i] = massets[i - sassets.size()].GetLocalisedName();
          tmpassets[i] = massets[i - sassets.size()];
        }else{
          ret[i] = sassets[i].GetLocalisedName();
          tmpassets[i]  = sassets[i];
        }
      }
      return ret;
    }
    return inherited(propertyID);
  }


  string GetPropertyName(string propertyID)
  {
    if(propertyID == "displace") return strTable.GetString("interface-offsetaxis");
    else if(propertyID == "vert_displ") return strTable.GetString("interface-offsetheight");
    else if(propertyID == "izojoin") return strTable.GetString("interface-izojoin");
    else if(propertyID[0,5] == "speed") return strTable.GetString("interface-speedboard");
    else if(propertyID == "twait") return strTable.GetString("interface-pointerdelay");
    else if(propertyID == "pause_bef_red") return strTable.GetString("interface-pause_bef_red");
    else if(propertyID == "along_displ") return strTable.GetString("interface-offsetalong");


//    else if(propertyID == "offsetAxis" or propertyID == "boxpositionx") return strTable.GetString("interface-offsetAxis");
//    else if(propertyID == "offsetHeight" or propertyID == "boxoffsetHeight") return strTable.GetString("interface-offsetHeight");
//    else if(propertyID == "izojoin") return strTable.GetString("interface-izojoin");
//    else if(propertyID == "boxcustom") return strTable.GetString("interface-box");
//    else if(propertyID == "boxposition") return strTable.GetString("interface-box-position");
//    else if(propertyID == "boxpositiony") return strTable.GetString("interface-box-position-y");
//    else if(propertyID == "boxrotate") return strTable.GetString("interface-box-rotate");
    return inherited(propertyID);
  }

  public string GetPropertyValue(string propertyID)
  {
    if(propertyID == "displace") return (string)displacement;
    else if(propertyID == "vert_displ") return (string)vert_displ;
    else if(propertyID[0,5] == "speed") return speed_soup.GetNamedTag(propertyID[6,7] + propertyID[8,]);
    else if(propertyID == "twait" and MU) return (string)MU.timeToWait;
    else if(propertyID == "pause_bef_red") return (string)pause_bef_red;
    else if(propertyID == "along_displ") return (string)along_displ;
//    else if(propertyID == "boxpositionx") return (string)boxPositionX;
//    else if(propertyID == "boxpositiony") return (string)boxPositionY;
//    else if(propertyID == "boxrotate") return (string)boxRotate;
//    else if(propertyID == "boxoffsetHeight") return (string)boxOffsetHeight;
    return inherited(propertyID);
  }





  public void LinkPropertyValue(string id)
  {
    if(id[0,9] == "code_freq"){
      int code = Str.ToInt(id[10,]);
      if(code == 8) code_freq = Logic.SetStat(code_freq, 8, !(code_freq & 8));
      else if(code == 0) code_freq = code_freq & 8;
      else code_freq = Logic.SetStat(code_freq & 8, code, true);
    }else if(id == "deleteizojoin"){
      izojoin = null;
      me.UpdateIzojoin();
    }else if(id == "izojoinreverse"){
      izojoinreverse = !izojoinreverse;
      me.UpdateIzojoin();
    }else if(id == "lens_kit"){
		  lens_kit_n++;
      train_open = false;
      shunt_open = false;
      string[] l_k_arr = me.GetLensKit();
		  if(lens_kit_n >= l_k_arr.size()) lens_kit_n = 0;
		  int[] pos_lins = new int[10];
		  bool[] ex_lins = new bool[10];
		  me.CreateLinsArr(lens_kit, ex_lins, pos_lins);
		  MC.RemoveMeshes(cast<MeshObject>me,  pos_lins);
		  lens_kit = l_k_arr[lens_kit_n];
		  me.CreateLinsArr(lens_kit, ex_lins, pos_lins);
		  if(head_conf != "") me.SetNewGolTex(ex_lins, pos_lins, true);
		  MC.MakeMeshes(cast<MeshObject>me, ex_lins, pos_lins , koz_mesh);
      Soup soup = me.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("lens-kits").GetNamedSoup(lens_kit_n);
      int ntype = soup.GetNamedTagAsInt("signaltype");
      if(ntype > 0) Type = ntype;
		  else Type = me.FindTypeByLens(ex_lins);
      if(soup.GetNamedTagAsBool("fourdigit")) ab4 = 1; else ab4 = 0;
		  LC.FindPossibleSgn(ex_sgn, ex_lins, prigl_enabled);
		  if(Type & ST_PROTECT){
			  if(ex_sgn[1]){
          ex_sgn[1] = false;
          pre_protected = false;
        }else{
				  int[] pos_lins = new int[10];
				  bool[] ex_lins = new bool[10];
				  me.CreateLinsArr(lens_kit, ex_lins, pos_lins);
				  LC.FindPossibleSgn(ex_sgn, ex_lins, prigl_enabled);
				  if(ex_sgn[1]){
            ex_sgn[1] = false;
            pre_protected = false;
          }else if(ex_sgn[6]){
            ex_sgn[6] = false;
            pre_protected = true;
          }
        }
      }
		  MainState = LC.FindSignalState(false, 0, ex_sgn, ab4, 0, train_open, shunt_open, false, 0);
		}else if(id == "kor_BU_1" or id == "kor_BU_2"){
      inherited(id);
      me.SetNewGolPosition(MU != null);
      me.SetBUArrow(true);
    }else if(id[0,9]=="displace1"){
 			displacement = Str.ToFloat(id[10,]);
      me.UpdateSignalPosition();
    }else if(id == "offsetalongdef"){
      along_displ = 0.0;
      me.UpdateSignalPosition();
    }
    else if(id == "offsetHeightlow") me.SetPropertyValue("vert_displ", -0.2);
    else if(id == "offsetHeightdef") me.SetPropertyValue("vert_displ", 0.0);
    else inherited(id);
  }

  void SetPropertyValue(string propertyID, string value, int index)
  {
    if(propertyID == "izojoin"){
      izojoin = tmpassets[index];
      izojoinreverse = false;
      me.UpdateIzojoin();
    }
    else inherited(propertyID, value, index);
  }

  void SetPropertyValue(string id, float val)
  {
	  if(id == "displace"){
		  displacement = val;
      me.UpdateSignalPosition();
	  }else if(id == "vert_displ"){
		  vert_displ = val;
      me.UpdateSignalPosition();
    }else if(id == "along_displ"){
      along_displ = val;
      me.UpdateSignalPosition();
    }else inherited(id, val);
  }







	public Soup GetProperties(void)
	{
		Soup soup = inherited();
    if(izojoin) soup.SetNamedTag("Izojoin", izojoin.GetKUID());
    if(izojoin) soup.SetNamedTag("IzojoinReverse", izojoinreverse);
    return soup;
  }






  public void SetProperties(Soup soup)
  {
    inherited(soup);
    stationName = soup.GetNamedTag("stationName");
    if(stationName != ""){
      string[] obj_p = new string[1];
      obj_p[0] = stationName;
      mainLib.LibraryCall("add_station", obj_p, null);
      obj_p[0] = null;
    }else{
      string default1 = mainLib.LibraryCall("station_edited_find", null, null);
      if(default1 != "") stationName = default1;
    }
    privateName = soup.GetNamedTag("privateName");
    me.ShowName(false);

    MainState = soup.GetNamedTagAsInt("MainState", 0);
    Type = soup.GetNamedTagAsInt("GetSignalType()", -1);
    ab4 = soup.GetNamedTagAsInt("ab4", -1);

    lens_kit_n = soup.GetNamedTagAsInt("lens_kit_n",0);
    if(lens_kit_n >= 0){
      Soup soup = me.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("lens-kits").GetNamedSoup(lens_kit_n);
      int stype = soup.GetNamedTagAsInt("signaltype", -1);
      string[] l_k_arr = me.GetLensKit();
      if(lens_kit_n < l_k_arr.size())
        lens_kit = l_k_arr[lens_kit_n];
      if(stype >= 0) Type = stype;
      if(ab4 < 0 and soup.GetNamedTagAsBool("fourdigit")) ab4 = 1;
      else if(ab4 < 0) ab4 = 0;
    }else lens_kit = soup.GetNamedTag("lens_kit");

    int[] pos_lins = new int[10];
    bool[] ex_lins = new bool[10];

    me.CreateLinsArr(lens_kit, ex_lins, pos_lins);

    dis_koz = soup.GetNamedTagAsBool("dis_koz", head_conf != "");

    if(head_conf != "" and dis_koz)
      me.SetNewGolTex(ex_lins, pos_lins, false);

    MC.MakeMeshes(cast<MeshObject>me, ex_lins, pos_lins, koz_mesh);

    OldMainState = -1;

    string ex_sign_1 = soup.GetNamedTag("ExSignals_str");
    if(ex_sign_1 == "") LC.FindPossibleSgn(ex_sgn, ex_lins, prigl_enabled);
    else ex_sgn = me.StrToExSignals(ex_sign_1);

    if(Type < 0) Type = FindTypeByLens(ex_lins);
    if(ab4 < 0) ab4 = 0;

    if(Type & ST_PERMOPENED) train_open = true;
    else if(Type & ST_SHUNT) train_open = false;
    else train_open = soup.GetNamedTagAsBool("train_open", false);
    shunt_open = soup.GetNamedTagAsBool("shunt_open", false);

	  if(Type & ST_PROTECT){
		  protect_influence = soup.GetNamedTagAsBool("protect_influence", true);
		  if(ex_sgn[1]){
        ex_sgn[1] = false;
        pre_protected = false;
		  }else{
        int[] pos_lins = new int[10];
        bool[] ex_lins = new bool[10];
			  me.CreateLinsArr(lens_kit, ex_lins, pos_lins);
			  LC.FindPossibleSgn(ex_sgn, ex_lins, prigl_enabled);
			  if(ex_sgn[1]){
          ex_sgn[1] = false;
          pre_protected = false;
			  }else if(ex_sgn[6]){
          ex_sgn[6] = false;
          pre_protected = true;
        }
			}
		  barrier_closed = soup.GetNamedTagAsBool("barrier_closed", false);
		  ProtectGroup = soup.GetNamedTag("ProtectGroup");
		  if(ProtectGroup != ""){
			  protect_soup = soup.GetNamedSoup("protect_soup");
			  if(protect_soup.IsLocked()){
          Soup sp1 = Constructors.NewSoup();
          sp1.Copy(protect_soup);
          protect_soup = sp1;
				}
        string[] return_str = new string[1];
        return_str[0] = ProtectGroup;
			  mainLib.LibraryCall("add_protect",return_str,GSO);
			}else protect_soup = Constructors.NewSoup();
	  }else{
      barrier_closed = false;
      pre_protected = false;
		}

    span_soup = soup.GetNamedSoup("span_soup");
    wrong_dir = soup.GetNamedTagAsBool("wrong_dir", false);

    train_is_l = soup.GetNamedTagAsBool("train_is_l", false);

    speed_soup = soup.GetNamedSoup("speed_soup");
    if(!speed_soup or !speed_soup.GetNamedTagAsBool("Inited", false))
      me.GetDefaultSignalLimits();

    displacement = soup.GetNamedTagAsFloat("displacement", me.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedTagAsFloat("default-position", 3.2));

//    def_displ = soup.GetNamedTagAsFloat("def_displ",0);
//    if(def_displ == 0){
//      def_displ = me.GetAsset().GetConfigSoup().GetNamedTagAsFloat("trackside", 0);
//      displacement = def_displ;
//    }

    vert_displ = soup.GetNamedTagAsFloat("vert_displ", 0);
	  along_displ = soup.GetNamedTagAsFloat("along_displ",0);
    //me.SetMeshTranslation("default", displacement - def_displ, 0, vert_displ);
    me.UpdateSignalPosition();

	  pause_bef_red = soup.GetNamedTagAsFloat("pause_bef_red", 1.5);

    if(isMacht){
      if(soup.GetNamedTagAsBool("MU_set", false)){
        if(!MU){
          MU = new zxRouter();
          MU.Owner = cast<Trackside>me;
          MU.OwnerSignal = cast<zxSignal>me;
        }
        MU.SetProperties(soup);
        MU.Init();
      }else if(MU){
        MU.Remove();
        MU = null;
      }
      kor_BU_1 = soup.GetNamedTagAsBool("kor_BU_1", false);
      kor_BU_2 = soup.GetNamedTagAsBool("kor_BU_2", false);
      me.MakeBUArrow();

      head_rot = soup.GetNamedTagAsFloat("head_rot", 0);
      head_krepl_rot = soup.GetNamedTagAsFloat("head_krepl_rot", 0);
      me.SetHeadRotation(true);
    }
    me.SetNewGolPosition(MU != null);

	  yellow_code = soup.GetNamedTagAsBool("yellow_code", false);

    if(Type & ST_IN) me.AddHandler(me, "SetSpanDirection", "", "OldSpanHandler");

    zxSP_name = soup.GetNamedTag("zxSPName");
    if(zxSP_name != "") zxSP = cast<zxSpeedBoard>Router.GetGameObject(zxSP_name);

	  MU_name = soup.GetNamedTag("MU_name");
	  if(MU_name != "") linkedMU= cast<Trackside>Router.GetGameObject(MU_name);

	  predvhod = soup.GetNamedTagAsBool("predvhod", false);
	  if(predvhod) me.SetPredvhod();

	  if((Type & ST_SHUNT) or (Type & ST_UNLINKED))
      code_freq = soup.GetNamedTagAsInt("code_freq",0);
	  else code_freq = soup.GetNamedTagAsInt("code_freq",2);

	  code_dev = soup.GetNamedTagAsInt("code_dev");
	  def_path_priority = soup.GetNamedTagAsInt("def_path_priority", 0);

	  AttachedJunction = soup.GetNamedTag("AttachedJunction");

	  if(station_edited and stationName != ""){
		  station_edited = false;
		  string[] obj_p = new string[1];
		  obj_p[0] = stationName;
		  mainLib.LibraryCall("station_edited_set", obj_p, null);
		  obj_p[0] = null;
		}

	  Inited = true;

    if(Type & ST_UNLINKED){
      if(Type & (ST_IN | ST_OUT) and !train_open){
        MainState = 1;
        me.SetSignalState(0, "");
      }else me.SetSignalState(2, "");
      LC.sgn_st[MainState].l.InitIndif(set_lens, set_blink);
      me.NewSignal(set_lens, 0, 0.7);
		  if(IsServer) mainLib.LibraryCall("mult_settings", null, GSO);
    }else me.UpdateState(0, -1);

    KUID izjkuid = soup.GetNamedTagAsKUID("Izojoin");
    if(izjkuid) izojoin = World.FindAsset(izjkuid);
    else izojoin = null;
		izojoinreverse = soup.GetNamedTagAsBool("IzojoinReverse");
    me.UpdateIzojoin();
	}


  public void AppendDependencies(KUIDList io_dependencies)
  {
    inherited(io_dependencies);
    if(izojoin) io_dependencies.AddKUID(izojoin.GetKUID());
  }


  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    Soup soup = myAsset.GetConfigSoup().GetNamedSoup("extensions");
    isMacht = soup.GetNamedTagAsBool("ismacht");
    if(isMacht){
      head_conf = soup.GetNamedTag("headconfig");
		  if(head_conf != "") gol_tex = myAsset.FindAsset("gol_tex");
		}else{
		  head_conf = "";
		  gol_tex = null;
    }
    devices = soup.GetNamedTagAsInt("devices");
    strTable = myAsset.FindAsset("extension-core").GetStringTable();
    if(soup.GetNamedTagAsBool("isled")){
      MC.dt_on = 0;
      MC.dt_off = 0;
    }
  }

};
