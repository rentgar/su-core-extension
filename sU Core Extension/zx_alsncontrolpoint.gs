include "zx_extension_library.gs"
include "alsn_circuitrepeater.gs"
include "interfaceprovider.gs"
include "trainzscript.gs"
include "string.gs"
include "Logic.gs"

class ZX_ALSNControlPoint isclass ALSN_CircuitRepeater, InterfaceProviderBase
{
  public define int IND_NONE       = 0; //АЛСН отключён, или нет кода
  public define int IND_GREEN      = 1; //Зелёный сигнал АЛСН
  public define int IND_YELLOW     = 2; //Жёлтый сигнал АЛСН
  public define int IND_REDYELLOW  = 4; //Красно-жёлтый сигнал АЛСН
  public define int IND_RED        = 8; //Красный сигнал АЛСН
  public define int IND_WHITE      = 16; //Белый сигнал АЛСН

  define int CFG_BASE              = 523776; //Базовые настройки

  ZX_Extension_Library library; //Ссылка на библиотеку

  //Состояние тестера:
  //1 - Указатель, что включён;
  //2 - Указтель, что в выключеном состоянии надо транслировать коды светофора;
  //4 - Частота кодирования 25,
  //8 - частота кодирования 50,
  //16 - частота кодирования - 75,
  //32 - Зелёный сигнал светофора
  //64 - Жёлтый сигнал светофора
  //128 - К/Ж
  //256 - Является внешним провайдером
  //512 - Позоаляет выбирать возможность быть внешним провайдером
  //1024 - Предоставляет возможность выбора стороннего изостыка
  //2048 - Предоставляет возможность разворачивать изостык
  //4096 - Разрешает двигать объект относительно оси пути
  //8192 - Разрешает двигать объект по вертикали
  //16384 - Разрешает вращать объект по оси Х
  //32768 - Разрешает вращать объект по оси Y
  //65536 - Разрешает вращать объект по оси Z
  //131072 - Разрешает разворот объекта по оси Z на 180 градусов
  //262144 - Автоматический реверс объекта
  //524288 - Указатель, что объект развёрнут
  //1048576 - Изостык развёрнут
  int status = 0;
  int linzhandlertype = 1; //0 - Обработчик линз отсутствует (нет внешних указателей), 1 - встроеный обработчик линз, 2 - сторонний обработчик линз (надо переопределить OnUpdateSignal(int signal))
  float offsetAxis; //Смещение от оси пути
  float offsetHeight; //Смещения по высоте
  float rotateX; //Поворот по оси Х
  float rotateY; //Поворот по оси Y
  float rotateZ; //Поворот по оси Z (Вокруг оси)
  float defaultOffsetAxis; //Смещение от сои пути по умолчанию
  float defaultRotateX; //Поворот по оси Х по умолчанию
  float defaultRotateY; //Поворот по оси Y по умолчанию
  float defaultRotateZ; //Поворот по оси Z по умолчанию
  int indicator = IND_NONE; //Показание индикатора

  string privateName = "";
  Asset izojoin;

  string renameError; //Ошибка переименовния

  Browser browser;
  Asset[] tmpassets;

  //Возникает при изменении состояния информационных маркеров
  //valid - указатель, что необходимые для работы датчика настройки выполнены
  //warning - указатель, что один из подключаемых внешних объектов не был найден
  void UpdateInfoMarkers(bool valid, bool warning)
  {
    MeshObject infomrk = me.GetFXAttachment("infomarker");
    if(infomrk){
      infomrk.SetMeshVisible("exclamation", !valid, 0.0);
      infomrk.SetMeshVisible("question", valid and warning, 0.0);
    }
  }

  final string GetIcon(string Name)
  {
    KUID kuid = library.GetAsset().FindAsset(Name).GetKUID();
    return HTMLWindow.MakeImage(kuid.GetHTMLString(), true);
  }

  void UpdateIzojoin(void)
  {
    MeshObject mo = me.GetFXAttachment("izojoin");
    if(izojoin and (!mo or mo.GetAsset() != izojoin))
      mo = me.SetFXAttachment("izojoin", me.izojoin);
    else if(!me.izojoin) mo = me.SetFXAttachment("izojoin", null);
    if(mo and status & 1048576)
      mo.SetMeshOrientation("default", 0.0, 0.0, Math.PI);
    else if(mo) mo.SetMeshOrientation("default", 0.0, 0.0, 0.0);
  }

  final void UpdateSignal(int signal)
  {
    MeshObject lgreen = me.GetFXAttachment("linz-green");
    MeshObject lyellow = me.GetFXAttachment("linz-yellow");
    MeshObject lredyellow = me.GetFXAttachment("linz-redyellow");
    MeshObject lred = me.GetFXAttachment("linz-red");
    MeshObject lwhite = me.GetFXAttachment("linz-white");
    if(signal & IND_GREEN) lgreen.SetMesh("linz-green-light", 0.1);
    else lgreen.SetMesh("linz-green", 0.1);
    if(signal & IND_YELLOW) lyellow.SetMesh("linz-yellow-light", 0.1);
    else lyellow.SetMesh("linz-yellow", 0.1);
    if(signal & IND_REDYELLOW) lredyellow.SetMesh("linz-redyellow-light", 0.1);
    else lredyellow.SetMesh("linz-redyellow", 0.1);
    if(signal & IND_RED) lred.SetMesh("linz-red-light", 0.1);
    else lred.SetMesh("linz-red", 0.1);
    if(signal & IND_WHITE) lwhite.SetMesh("linz-white-light", 0.1);
    else lwhite.SetMesh("linz-white", 0.1);
  }

  void OnUpdateSignal(int signal){}

  final float GradToRad(float grad){return grad * Math.PI / 180;}//Перевод градусов в радианы
  final float RadToGard(float rad){return rad * 180 / Math.PI;}//Перевод радиан в градусы

  //Обновление положения объекта в пространстве
  //Доступна для переопределения
  void UpdateObjectOffset(void)
  {
    me.SetMeshTranslation("default", offsetAxis, 0, offsetHeight);
    float rotAngleX = defaultRotateX;
    float rotAngleY = defaultRotateY;
    float rotAngleZ = defaultRotateZ;
    if(status & 393216){
      if(status & 524288) rotAngleZ = rotAngleZ + Math.PI;
      int rev = rotAngleZ / (2 * Math.PI);
      rotAngleZ = rotAngleZ - rev * 2 * Math.PI;
    }
    if(status & 16384) rotAngleX = rotAngleX + -rotateX;
    if(status & 32768) rotAngleY = rotAngleY + rotateY;
    if(status & 65536) rotAngleZ = rotAngleZ + rotateZ;
    me.SetMeshOrientation("default", rotAngleX, rotAngleY, rotAngleZ);

//    if(linzhandlertype = 1) me.UpdateSignal(IND_NONE);
//    else if(linzhandlertype = 2) me.OnUpdateSignal(IND_NONE);
  }

  //Извлечение внутреннего имени
  public final string GetPrivateName(void){return privateName;}

  public final bool IsOutsideProvider(void){return (bool)(status & 256);}

  public final int GetIndicatorCode(void){return indicator;}

  //Указатель, что повторитель инициализирован и работает
  public final bool Enabled(void){return privateName != "" and (status & 1 or !(status & 2));}

  //Получение провайдера, для которого выполняется повторение
  public final ALSN_Provider ControlProvider(void){return cast<ALSN_Provider>((object)me);}

  //Получение кода АЛСН.
  public final int GetALSNCode(void)
  {
    if(privateName != "" and status & 1 and status & 28){
      if(status & 32) return CODE_GREEN;
      else if(status & 64) return CODE_YELLOW;
      else if(status & 128) return CODE_REDYELLOW;
    }
    return CODE_NONE;
  }

  //Получение частоты кодирования сигнала
  public int GetALSNFrequency(void)
  {
    if(privateName != "" and status & 1) return (status >> 2) & (FREQ_ALSN25 | FREQ_ALSN50 | FREQ_ALSN75);
    return FREQ_NONE;
  }

  //Установка частоты кодирования сигнала
  public void SetFrequency(int frequency)
  {
    if(privateName != ""){
      status = Logic.SetStat(status, 4, frequency == FREQ_ALSN25);
      status = Logic.SetStat(status, 8, frequency == FREQ_ALSN50);
      status = Logic.SetStat(status, 16, frequency == FREQ_ALSN75);
      status = Logic.SetStat(status, 1, status & 28);
      if(!(status & 1)){
        status = Logic.SetStat(status, 224, false);
        indicator = IND_NONE;
      }else if(indicator == IND_NONE) indicator = IND_WHITE;
      if(linzhandlertype == 1) me.UpdateSignal(indicator);
      else if(linzhandlertype == 2) me.OnUpdateSignal(indicator);
      me.PostMessage(null, "ZXALSNCONTROLPOINT", "Update", 0.0);
    }
  }

  public void SetCode(int code)
  {
    if(privateName != "" and status & 1 and status & 28){
      int ccode = me.GetALSNCode();
      status = Logic.SetStat(status, 32, code == CODE_GREEN);
      status = Logic.SetStat(status, 64, code == CODE_YELLOW);
      status = Logic.SetStat(status, 128, code == CODE_REDYELLOW);
      if((ccode == CODE_GREEN or ccode == CODE_YELLOW) and !(status & 224)) indicator = IND_WHITE;
      else if(ccode == CODE_REDYELLOW and !(status & 224)) indicator = IND_RED;
      else if(status & 32) indicator = IND_GREEN;
      else if(status & 64) indicator = IND_YELLOW;
      else if(status & 128) indicator = IND_REDYELLOW;
      if(linzhandlertype == 1) me.UpdateSignal(indicator);
      else if(linzhandlertype == 2) me.OnUpdateSignal(indicator);
      me.PostMessage(null, "ZXALSNCONTROLPOINT", "Update", 0.0);
    }
  }

  final string GetStringBool(bool stat)
  {
    if(stat) return strTable.GetString("yes");
    return strTable.GetString("no");
  }

  final string GetStringIzojoinType(void)
  {
    if(status & 1048576) return strTable.GetString("yes");
    else if(status & 2097152) return strTable.GetString("yes");
    return strTable.GetString("no");
  }

	public string GetPropertyType(string propertyID)
	{
    if(propertyID == "rename") return "string,1,30";
    else if(propertyID == "repeater" or propertyID == "provider" or propertyID == "setoffsetaxisleft" or propertyID == "setoffsetaxisright" or propertyID == "setoffsetheightlow" or
    propertyID == "setoffsetheightdef" or propertyID == "reverse" or propertyID == "setrotatexdefault" or propertyID == "setrotateydefault" or propertyID == "setrotatezdefault" or
    propertyID == "deleteizojoin" or propertyID == "izojoinreverse") return "link";
		else if(propertyID == "setoffsetaxis" or propertyID == "setoffsetheight") return "float,-100.0,100.0,0.1";
		else if(propertyID == "setrotatex" or propertyID == "setrotatey") return "float,-90.0,90.0,0.5";
		else if(propertyID == "setrotatez") return "float,-180.0,180.0,1.0";
    else if(propertyID == "izojoin") return "list,1";
    return inherited(propertyID);
  }

  string GetPropertyName(string propertyID)
  {
    if(propertyID == "rename") return strTable.GetString("interface-setname");
    else if(propertyID == "setoffsetaxis") return strTable.GetString("interface-offsetaxis");
    else if(propertyID == "setoffsetheight") return strTable.GetString("interface-offsetheight");
    else if(propertyID == "setrotatex" or propertyID == "setrotatey") return strTable.GetString("interface-rotate");
    else if(propertyID == "setrotatez") return strTable.GetString("interface-rotatez");
    else if(propertyID == "izojoin") return strTable.GetString("interface-izojoin");
    return inherited(propertyID);
  }

  public string GetPropertyValue(string propertyID)
  {
    if(propertyID == "rename") return privateName;
    else if(propertyID == "setoffsetaxis")return (string)offsetAxis;
    else if(propertyID == "setoffsetheight")return (string)offsetHeight;
    else if(propertyID == "setrotatex")return (string)me.RadToGard(rotateX);
    else if(propertyID == "setrotatey")return (string)me.RadToGard(rotateY);
    else if(propertyID == "setrotatez")return (string)me.RadToGard(rotateZ);
    return inherited(propertyID);
  }

  public string[] GetPropertyElementList(string propertyID)
  {
    if(propertyID == "izojoin"){
      Asset[] sassets = TrainzScript.GetAssetList("scenary", "TSIZJ");
      Asset[] massets = TrainzScript.GetAssetList("mesh", "TSIZJ");
      string[] ret = new string[sassets.size() + massets.size()];
      tmpassets = new Asset[sassets.size() + massets.size()];
      int i;
      for(i = 0; i < sassets.size() + massets.size(); i++){
        if(i >= sassets.size()){
          ret[i] = massets[i - sassets.size()].GetLocalisedName();
          tmpassets[i] = massets[i - sassets.size()];
        }else{
          ret[i] = sassets[i].GetLocalisedName();
          tmpassets[i]  = sassets[i];
        }
      }
      return ret;
    }
    return inherited(propertyID);
  }


  public string GetDescriptionHTML(void)
  {
    StringTable strTableObj = me.GetAsset().GetStringTable();
    string title = strTableObj.GetString("title");
    if(title == "") title = strTable.GetString("title-alsnpoint");
    string ret = me.MakeTitle(title), ttrc, ttop;
    int celltype = CELL_VALUE;//, i;
    if(privateName == "") celltype = CELL_ERROR;
    if(status & 2) ttrc = "off"; else ttrc = "on";
    if(status & 256) ttop = "off"; else ttop = "on";



    ret = ret + HTMLWindow.StartTable("border=0 width=400");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-general"), "colspan=2", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("rename", strTable.GetString("name") + ":", strTable.GetString("tooltip-setname-alsn"), "width=120", CELL_KEY) +
                me.MakeLinkCell("rename", privateName, strTable.GetString("tooltip-setname-alsn"), "", celltype) + HTMLWindow.EndRow();
    if(renameError != "")ret = ret + HTMLWindow.StartRow() + me.MakeCell(renameError, "colspan=2 align=center", CELL_ERROR) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("repeatcircuit") + ":", "colspan", CELL_KEY) +
                me.MakeLinkCell("repeater", me.GetStringBool(status & 2), strTable.GetString("tooltip-repeatcircuit-" + ttrc), "colspan", CELL_VALUE) + HTMLWindow.EndRow();
    if(status & 512){
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("outsideprovider") + ":", "colspan", CELL_KEY) +
                  me.MakeLinkCell("provider", me.GetStringBool(status & 256), strTable.GetString("tooltip-outsideprovider-" + ttop), "colspan", CELL_VALUE) + HTMLWindow.EndRow();
    }
    ret = ret + HTMLWindow.EndTable() + "<br />";

    if(status & 258048){
      ret = ret + HTMLWindow.StartTable("border='0' width='400'");
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-offset"), "colspan ='4'", CELL_HEAD) + HTMLWindow.EndCell() + HTMLWindow.EndRow();
      if(status & 4096)
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("offsetaxis") + ":", "width='220'", CELL_KEY) +
                    me.MakeLinkCell("setoffsetaxis", me.GetCleanFloatString(offsetAxis), strTable.GetString("tooltip-offsetaxis"), "width=60 align=center", CELL_VALUE) +
                    me.MakeLinkCell("setoffsetaxisleft", me.GetCleanFloatString(-defaultOffsetAxis), strTable.GetString("tooltip-offsetaxis-defleft"), "width=60 align=center", CELL_KEY) +
                    me.MakeLinkCell("setoffsetaxisright", me.GetCleanFloatString(defaultOffsetAxis), strTable.GetString("tooltip-offsetaxis-defright"), "width=60 align=center", CELL_KEY)+
                    HTMLWindow.EndRow();
      if(status & 8192)
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("offsetheight") + ":", "width='220'", CELL_KEY) +
                    me.MakeLinkCell("setoffsetheight", me.GetCleanFloatString(offsetHeight), strTable.GetString("tooltip-offsetheight"), "width=60 align=center", CELL_VALUE) +
                    me.MakeLinkCell("setoffsetheightlow", "-0.20", strTable.GetString("tooltip-offsetheightlow"), "width=60 align=center", CELL_KEY) +
                    me.MakeLinkCell("setoffsetheightdef", "0.00", strTable.GetString("tooltip-offsetheightdef"), "width=60 align=center", CELL_KEY) +
                    HTMLWindow.EndRow();
      if(status & 16384)
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("rotatex") + ":", "width='220'", CELL_KEY) +
                    me.MakeLinkCell("setrotatex", me.GetCleanFloatString(me.RadToGard(rotateX)), strTable.GetString("tooltip-setrotatex"), "width=60 align=center", CELL_VALUE) +
                    me.MakeLinkCell("setrotatexdefault", strTable.GetString("rotate-default"), strTable.GetString("tooltip-resetrotatex"), "align='center' colspan ='2'", CELL_KEY) +
                    HTMLWindow.EndRow();
      if(status & 32768)
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("rotatey") + ":", "width='220'", CELL_KEY) +
                    me.MakeLinkCell("setrotatey", me.GetCleanFloatString(me.RadToGard(rotateY)), strTable.GetString("tooltip-setrotatey"), "width=60 align=center", CELL_VALUE) +
                    me.MakeLinkCell("setrotateydefault", strTable.GetString("rotate-default"), strTable.GetString("tooltip-resetrotatey"), "align='center' colspan ='2'", CELL_KEY) +
                    HTMLWindow.EndRow();
      if(status & 65536)
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("rotatez") + ":", "width='220'", CELL_KEY) +
                    me.MakeLinkCell("setrotatez", me.GetCleanFloatString(me.RadToGard(rotateZ)), strTable.GetString("tooltip-setrotatez"), "width=60 align=center", CELL_VALUE) +
                    me.MakeLinkCell("setrotatezdefault", strTable.GetString("rotate-default"), strTable.GetString("tooltip-resetrotatez"), "align='center' colspan ='2'", CELL_KEY) +
                    HTMLWindow.EndRow();
      if(status & 131072)
        ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("reverse", strTable.GetString("reverse"), strTable.GetString("tooltip-reverse"), status & 524288, "colspan='4'", CELL_KEY) +
                    HTMLWindow.EndRow();
      ret = ret + HTMLWindow.EndTable() + "<br />";
    }

    if(status & 1024){
      string izjname;
      if(izojoin) izjname = izojoin.GetLocalisedName();
      ret = ret + HTMLWindow.StartTable("border='0' width='400'");
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-outside"), "colspan ='3'", CELL_HEAD) + HTMLWindow.EndCell() + HTMLWindow.EndRow();
      ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("izojoin", strTable.GetString("izojoin") + ":", strTable.GetString("tooltip-selectizojoin"), "width=220", CELL_KEY) +
                  me.MakeLinkCell("izojoin", izjname, strTable.GetString("tooltip-selectizojoin"), "", CELL_VALUE);
      if(izojoin) ret = ret + me.MakeLinkCell("deleteizojoin", me.GetIcon("icon-delete"), strTable.GetString("tooltip-deleteizojoin"), "width=16", CELL_VALUE) + HTMLWindow.EndRow();
      else ret = ret + me.MakeCell(me.GetIcon("icon-delete-d"), "width=16", CELL_VALUE) + HTMLWindow.EndRow();
      if(status & 2048 and izojoin){
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("reverse-izojoin") + ":", "width=220", CELL_KEY) +
                    me.MakeLinkCell("izojoinreverse", me.GetStringBool(status & 1048576), strTable.GetString("tooltip-reverse-izojoin"), "colspan=2", CELL_VALUE) + HTMLWindow.EndRow();
      }
      ret = ret + HTMLWindow.EndTable() + "<br />";
    }

    renameError = "";
    tmpassets = null;
    return ret;
  }

  void SetPropertyValue(string propertyID, string value)
  {
    Str.TrimLeft(value, " ");
    Str.TrimRight(value, " ");
    String.Left(value, 30);
    if(propertyID == "rename"){
      if(value != ""){
        bool can = !library.ALSNPointNameContains(value);
        if(!can) renameError = strTable.GetString1("error-alsncontrolpoint-alreadyexist", value);
        else privateName = value;
      }else renameError = strTable.GetString1("error-alsncontrolpoint-empty", value);
      me.UpdateInfoMarkers(privateName != "", false);
    }else inherited(propertyID, value);
  }

  void SetPropertyValue(string propertyID, float value)
  {
    if(propertyID == "setoffsetaxis"){
      if(status & 262144 and value < 0 and offsetAxis >= 0) status = Logic.SetStat(status, 524288, true);
      else if(status & 262144 and value >= 0 and offsetAxis < 0) status = Logic.SetStat(status, 524288, false);
      offsetAxis = value;
      me.UpdateObjectOffset();
    }else if(propertyID == "setoffsetheight"){
      offsetHeight = value;
      me.UpdateObjectOffset();
    }else if(propertyID == "setrotatex"){
      rotateX = me.GradToRad(value);
      me.UpdateObjectOffset();
    }else if(propertyID == "setrotatey"){
      rotateY = me.GradToRad(value);
      me.UpdateObjectOffset();
    }else if(propertyID == "setrotatez"){
      rotateZ = me.GradToRad(value);
      me.UpdateObjectOffset();
    }else inherited(propertyID, value);
  }

  void SetPropertyValue(string propertyID, string value, int index)
  {
    if(propertyID == "izojoin"){
      izojoin = tmpassets[index];
      status = Logic.SetStat(status, 1048576, false);
      me.UpdateIzojoin();
    }
    else inherited(propertyID, value, index);
  }


  void LinkPropertyValue(string propertyID)
  {
    if(propertyID == "repeater") status = Logic.SetStat(status, 2, !(status & 2));
    else if(propertyID == "provider") status = Logic.SetStat(status, 256, !(status & 256));
    else if(propertyID == "setoffsetaxisleft"){
      if(status & 262144 and offsetAxis >= 0) status = Logic.SetStat(status, 524288, true);
      offsetAxis = -defaultOffsetAxis;
      me.UpdateObjectOffset();
    }else if(propertyID == "setoffsetaxisright"){
      if(status & 262144 and offsetAxis < 0) status = Logic.SetStat(status, 524288, false);
      offsetAxis = defaultOffsetAxis;
      me.UpdateObjectOffset();
    }else if(propertyID == "setoffsetheightlow"){
      offsetHeight = -0.2;
      me.UpdateObjectOffset();
    }else if(propertyID == "setoffsetheightdef"){
      offsetHeight = 0.0;
      me.UpdateObjectOffset();
    }else if(propertyID == "reverse"){
      status = Logic.SetStat(status, 524288, status & 131072 and !(status & 524288));
      me.UpdateObjectOffset();
    }else if(propertyID == "setrotatexdefault"){
      rotateX = 0.0;
      me.UpdateObjectOffset();
    }else if(propertyID == "setrotateydefault"){
      rotateY = 0.0;
      me.UpdateObjectOffset();
    }else if(propertyID == "setrotatezdefault"){
      rotateZ = 0.0;
      me.UpdateObjectOffset();
    }else if(propertyID == "deleteizojoin"){
      izojoin = null;
      me.UpdateIzojoin();
    }else if(propertyID == "izojoinreverse"){
      status = Logic.SetStat(status, 1048576, status & 2048 and !(status & 1048576));
      me.UpdateIzojoin();
    }
    else inherited(propertyID);
  }

	public Soup GetProperties(void)
	{
		Soup soup = inherited();
    soup.SetNamedTag("privatename", privateName);
    soup.SetNamedTag("status", status & ~CFG_BASE);
    soup.SetNamedTag("indicator", indicator);
    soup.SetNamedTag("offsetaxis", offsetAxis);
    soup.SetNamedTag("offsetheight", offsetHeight);
    soup.SetNamedTag("rotatex", rotateX);
    soup.SetNamedTag("rotatey", rotateY);
    soup.SetNamedTag("rotatez", rotateZ);
    if(izojoin) soup.SetNamedTag("izojoin", izojoin.GetKUID());
    return soup;
  }

	public void SetProperties(Soup soup)
  {
    inherited(soup);
    privateName = soup.GetNamedTag("privatename");
    status = (status & CFG_BASE) | soup.GetNamedTagAsInt("status", status & ~CFG_BASE);
    indicator = soup.GetNamedTagAsInt("indicator", indicator);
    offsetAxis = soup.GetNamedTagAsFloat("offsetaxis", offsetAxis);
    offsetHeight = soup.GetNamedTagAsFloat("offsetheight", offsetHeight);
    rotateX = soup.GetNamedTagAsFloat("rotatex", rotateX);
    rotateY = soup.GetNamedTagAsFloat("rotatey", rotateY);
    rotateZ = soup.GetNamedTagAsFloat("rotatez", rotateZ);
    KUID izjkuid = soup.GetNamedTagAsKUID("izojoin");
    if(izjkuid) izojoin = World.FindAsset(izjkuid);
    else izojoin = null;
    me.UpdateIzojoin();
    me.UpdateObjectOffset();
    me.UpdateInfoMarkers(privateName != "", false);
    if(linzhandlertype == 1) me.UpdateSignal(indicator);
    else if(linzhandlertype == 2) me.OnUpdateSignal(indicator);
  }

  public void AppendDependencies(KUIDList io_dependencies)
  {
    inherited(io_dependencies);
    if(izojoin) io_dependencies.AddKUID(izojoin.GetKUID());
  }

  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    library = cast<ZX_Extension_Library>TrainzScript.GetLibrary(myAsset.LookupKUIDTable("extension-core-library"));
    strTable = library.GetAsset().GetStringTable();
    Soup config = myAsset.GetConfigSoup().GetNamedSoup("extensions");
    linzhandlertype = config.GetNamedTagAsInt("signal-handler-type", 1);
    status = Logic.SetStat(status, (config.GetNamedTagAsInt("outsideprovider-type", 2) & 3) << 8, true);
    status = Logic.SetStat(status, 1024, config.GetNamedTagAsBool("canizojoin", false));
    status = Logic.SetStat(status, 2048, config.GetNamedTagAsBool("izojoin-canreverse", status & 1024));
    status = Logic.SetStat(status, (config.GetNamedTagAsInt("canmove", 3) & 3) << 12, true);
    status = Logic.SetStat(status, (config.GetNamedTagAsInt("canrotate", 0) & 7) << 14, true);
    status = Logic.SetStat(status, 131072, config.GetNamedTagAsBool("canreverse", false));
    status = Logic.SetStat(status, 262144, config.GetNamedTagAsBool("autoreverse", false));
    offsetAxis = config.GetNamedTagAsFloat("startoffsetaxis", 3.1);
    defaultOffsetAxis = Math.Fabs(config.GetNamedTagAsFloat("defaultoffsetaxis", 3.1));

    string[] defRot = Str.Tokens(myAsset.GetConfigSoup().GetNamedSoup("mesh-table").GetNamedSoup("default").GetNamedTag("orientation"), ",");
    if(defRot.size() == 3){
      defaultRotateX = Str.ToFloat(defRot[0]);
      defaultRotateY = Str.ToFloat(defRot[1]);
      defaultRotateZ = Str.ToFloat(defRot[2]);
    }

    if(linzhandlertype < 0 or linzhandlertype > 2) linzhandlertype = 0;

    if(linzhandlertype == 1) me.UpdateSignal(IND_NONE);
    else if(linzhandlertype == 2) me.OnUpdateSignal(IND_NONE);
    library.RegisterALSNPoint(me);
  }


};