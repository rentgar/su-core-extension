include "zx_extension_library.gs"

class ZX_ALSNControlPoint_Indicator isclass MapObject, InterfaceProviderBase
{
  define int IND_NONE       = 0; //АЛСН отключён, или нет кода
  define int IND_GREEN      = 1; //Зелёный сигнал АЛСН
  define int IND_YELLOW     = 2; //Жёлтый сигнал АЛСН
  define int IND_REDYELLOW  = 4; //Красно-жёлтый сигнал АЛСН
  define int IND_RED        = 8; //Красный сигнал АЛСН
  define int IND_WHITE      = 16; //Белый сигнал АЛСН

  ZX_Extension_Library library; //Ссылка на библиотеку
  ZX_ALSNControlPoint provider; //Ссылка на провайдер
  int provider_id; //Игровой идентификатор провайдера

  bool customSignalUpdate; //Указатель, что переопределённый обработчик сигнала

  ZX_ALSNControlPoint[] tmpPointers; //Временный масив с датчиками
  int[] tmpIndexLink; //Временный масив с сопоставлением индексов
  int[] tmpPointersId; //Временный масив для хранения идентификаторов датчика

  final string GetIcon(string Name)
  {
    KUID kuid = library.GetAsset().FindAsset(Name).GetKUID();
    return HTMLWindow.MakeImage(kuid.GetHTMLString(), true);
  }

  //Возникает при изменении состояния информационных маркеров
  //valid - указатель, что необходимые для работы датчика настройки выполнены
  //warning - указатель, что один из подключаемых внешних объектов не был найден
  void UpdateInfoMarkers(bool valid, bool warning)
  {
    MeshObject infomrk = me.GetFXAttachment("infomarker");
    if(infomrk){
      infomrk.SetMeshVisible("exclamation", !valid, 0.0);
      infomrk.SetMeshVisible("question", valid and warning, 0.0);
    }
  }

  final void UpdateSignal(int signal)
  {
    MeshObject lgreen = me.GetFXAttachment("linz-green");
    MeshObject lyellow = me.GetFXAttachment("linz-yellow");
    MeshObject lredyellow = me.GetFXAttachment("linz-redyellow");
    MeshObject lred = me.GetFXAttachment("linz-red");
    MeshObject lwhite = me.GetFXAttachment("linz-white");
    if(signal & IND_GREEN) lgreen.SetMesh("linz-green-light", 0.1);
    else lgreen.SetMesh("linz-green", 0.1);
    if(signal & IND_YELLOW) lyellow.SetMesh("linz-yellow-light", 0.1);
    else lyellow.SetMesh("linz-yellow", 0.1);
    if(signal & IND_REDYELLOW) lredyellow.SetMesh("linz-redyellow-light", 0.1);
    else lredyellow.SetMesh("linz-redyellow", 0.1);
    if(signal & IND_RED) lred.SetMesh("linz-red-light", 0.1);
    else lred.SetMesh("linz-red", 0.1);
    if(signal & IND_WHITE) lwhite.SetMesh("linz-white-light", 0.1);
    else lwhite.SetMesh("linz-white", 0.1);
  }

  void OnUpdateSignal(int signal){}

	public string GetPropertyType(string propertyID)
	{
    if(propertyID == "provider") return "list,1";
    else if(propertyID == "deleteprovider") return "link";
    return inherited(propertyID);
  }

  string GetPropertyName(string propertyID)
  {
    if(propertyID == "provider") return strTable.GetString("interface-provider");
    return inherited(propertyID);
  }

  public string[] GetPropertyElementList(string propertyID)
  {
    if(propertyID == "provider"){
      int i, count = library.GetALSNPointCount();
      string[] ret = new string[0];
      tmpPointers = library.GetALSNPointList();
      tmpPointersId = new int[0];
      tmpIndexLink = new int[0];
      for(i; i < count; i++){
        if(library.CheckALSNPointValid(i) and tmpPointers[i].IsOutsideProvider()and tmpPointers[i].GetPrivateName() != ""){
          tmpPointersId[tmpPointersId.size()] = tmpPointers[i].GetId();
          ret[ret.size()] = tmpPointers[i].GetPrivateName();
          tmpIndexLink[tmpIndexLink.size()] = i;
        }
      }
      return ret;
    }
    return inherited(propertyID);
  }


  public string GetDescriptionHTML(void)
  {
    tmpPointers = null; tmpIndexLink = null; tmpPointersId = null;
    StringTable strTableObj = me.GetAsset().GetStringTable();
    string title = strTableObj.GetString("title");
    if(title == "") title = strTable.GetString("title-alsnpoint-ind");
    string ret = me.MakeTitle(title), pname = ""; int type = CELL_ERROR;
    if(provider){pname = provider.GetPrivateName(); type = CELL_VALUE;}
    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-general"), "colspan ='3'", CELL_HEAD) + HTMLWindow.EndCell() + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("provider", strTable.GetString("provider") + ":", strTable.GetString("tooltip-provider"), "width=120", CELL_KEY) +
                me.MakeLinkCell("provider", pname, strTable.GetString("tooltip-provider"), "", type);
    if(provider) ret = ret + me.MakeLinkCell("deleteprovider", me.GetIcon("icon-delete"), strTable.GetString("tooltip-deleteprovider"), "width=16", CELL_VALUE) + HTMLWindow.EndRow();
    else ret = ret + me.MakeCell(me.GetIcon("icon-delete-d"), "width=16", CELL_VALUE) + HTMLWindow.EndRow();
    return ret;
  }

  void SetPropertyValue(string propertyID, string value, int index)
  {
    if(propertyID == "provider"){
      ZX_ALSNControlPoint pointer = tmpPointers[tmpIndexLink[index]];
      provider = pointer; provider_id = pointer.GetId();
      me.UpdateInfoMarkers(pointer and true, false);
    }
  }

  void LinkPropertyValue(string propertyID)
  {
    if(propertyID == "deleteprovider") provider = null;
    else inherited(propertyID);
  }

	public Soup GetProperties(void)
	{
		Soup soup = inherited();
    if(provider and provider == Router.GetGameObject(provider_id) and
    provider.GetPrivateName() != "" and provider.IsOutsideProvider())
		  soup.SetNamedTag("Provider", provider.GetName());
    else provider = null;
    me.UpdateInfoMarkers(provider and true, false);
		return soup;
	}

  public void SetProperties(Soup soup)
	{
		inherited(soup);
    string pointerName = soup.GetNamedTag("Provider");
    if(pointerName != ""){
      ZX_ALSNControlPoint pnt = cast<ZX_ALSNControlPoint>Router.GetGameObject(pointerName);
      if(pnt and pnt.GetPrivateName() != "" and pnt.IsOutsideProvider()){
        provider = pnt;
        provider_id = pnt.GetId();
      } else provider = null;
    }else provider = null;
    me.UpdateInfoMarkers(provider and true, false);
    if(provider){
      if(customSignalUpdate) me.OnUpdateSignal(provider.GetIndicatorCode());
      else me.UpdateSignal(provider.GetIndicatorCode());
    }else{
      if(customSignalUpdate) me.OnUpdateSignal(IND_NONE);
      else me.UpdateSignal(IND_NONE);
    }
  }

  final void UpdatePointHandler(Message msg)
  {
    if(provider == msg.src) {
      if(customSignalUpdate) me.OnUpdateSignal(provider.GetIndicatorCode());
      else me.UpdateSignal(provider.GetIndicatorCode());
    }
  }




  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    library = cast<ZX_Extension_Library>TrainzScript.GetLibrary(myAsset.LookupKUIDTable("extension-core-library"));
    strTable = library.GetAsset().GetStringTable();
    Soup config = myAsset.GetConfigSoup().GetNamedSoup("extensions");
    customSignalUpdate = config.GetNamedTagAsBool("signal-handler-custom", false);
    if(customSignalUpdate) me.OnUpdateSignal(IND_NONE);
    else me.UpdateSignal(IND_NONE);
    me.AddHandler(me, "ZXALSNCONTROLPOINT", "Update", "UpdatePointHandler");
  }


};