include "zx_alsncontrolpoint_console.gs"
include "zx_alsncontrolpoint.gs"
include "library.gs"

class ZX_Extension_Library isclass Library
{
  StringTable strTable;


  ZX_ALSNControlPoint_Console alsn_point_console;
  int alsn_point_console_id;


  ZX_ALSNControlPoint[] alsn_points = new ZX_ALSNControlPoint[0];
  int[] alsn_points_id = new int[0];

  public final thread void RemoveAllInvalidateALSNPoints(void)
  {
    int i = 0;
    while(i < alsn_points.size()){
      bool valid = alsn_points[i] and alsn_points[i] == Router.GetGameObject(alsn_points_id[i]);
      if(!valid){
        alsn_points[i, i + 1] = null;
        alsn_points_id[i, i + 1] = null;
      }else i++;
    }
  }

  public final bool CheckALSNPointValid(int index)
  {
    bool valid = alsn_points[index] and alsn_points[index] == Router.GetGameObject(alsn_points_id[index]);
    if(!valid) me.RemoveAllInvalidateALSNPoints();
    return valid;
  }

  public int GetALSNPointCount(void){return alsn_points.size();}
  public ZX_ALSNControlPoint GerALSNPoint(int index){return alsn_points[index];}


  public void RegisterALSNPoint(ZX_ALSNControlPoint point)
  {
    alsn_points[alsn_points.size()] = point;
    alsn_points_id[alsn_points_id.size()] = point.GetId();
  }

  public bool ALSNPointNameContains(string name)
  {
    int i;
    for(i = 0; i < alsn_points.size(); i++)
      if(me.CheckALSNPointValid(i) and alsn_points[i].GetPrivateName() == name) return true;
    return false;
  }


  public ZX_ALSNControlPoint[] GetALSNPointList(void)
  {
    ZX_ALSNControlPoint[] ret;
    ret.copy(alsn_points);
    return ret;
  }


  public final ZX_ALSNControlPoint_Console GetALSNPointConsole(void)
  {
    if(alsn_point_console and alsn_point_console == Router.GetGameObject(alsn_point_console_id)) return alsn_point_console;
    return null;
  }

  //Установка правила библиотеки сообщений
  public final void SetALSNPointConsole(ZX_ALSNControlPoint_Console console)
  {
    alsn_point_console = console;
    alsn_point_console_id = console.GetId();
  }
















  final void OnClickSystemButton(Message msg)
  {
    Menu iconMenu = Constructors.NewMenu();
    iconMenu.AddItem(strTable.GetString("alsnconsole"), alsn_point_console, "ZXALSNCONTROLPOINT", "ConsoleOpen");
//    if(MultiplayerGame.IsActive() and MultiplayerGame.IsServer()){
//      iconMenu.AddSeperator();
//      iconMenu.AddItem(strTable.GetString("showadmin"), controller, "ZXDSP", "Admin");
//    }
    me.ShowSystemMenuIconMenu(iconMenu);
	}

	final void ModuleInitHandler(Message msg)
	{
	  if(World.GetCurrentModule() == World.DRIVER_MODULE and alsn_point_console){
		  me.AddSystemMenuIcon(GetAsset().FindAsset("menuicon"), strTable.GetString("tooltip"), "");
      //me.WaitMultiplayerActive();
    }
	}

  public void Init(Asset myAsset)
  {
    inherited(myAsset);
		me.RemoveSystemMenuIcon();
   	me.AddHandler(me, "World", "ModuleInit","ModuleInitHandler");
   	me.AddHandler(me, "Interface", "ClickSystemButton", "OnClickSystemButton");
    strTable = myAsset.GetStringTable();
  }



};