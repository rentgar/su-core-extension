include "zx_signalextension.gs"



class zxHangerSignal isclass zxSignalExtension
{
  define int CFG_TABLEMACHT       = 1; //Таблички крепяться к мачте
  define int CFG_TABLECRADLE      = 2; //Таблички крепяться к люльке
  define int CFG_TABLEAUTO        = CFG_TABLEMACHT | CFG_TABLECRADLE; //Автоматический выбор расположения табличек
  define int CGF_PREFDVHODCRADLE  = 4; //Указатель, что табличка предвходного светофора должна крепиться на люльку



  int[] tablecount = new int[2]; //Количество табличек: 0 - на мачте, 1 - на люльке

  int config = 0; //Тут будут режать настройки и состояние светофора

  void UpdateSignalPosition(void)
  {
    float height = me.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedTagAsFloat("cradle-height");
    me.SetMeshTranslation("default", displacement, along_displ, height + vert_displ);
  }

  MeshObject AddTabl(string effect)
  {
    MeshObject ret = me.GetFXAttachment(effect);
    if(!ret) ret = me.SetFXAttachment(effect, tabl_m);
    return ret;
  }

  public void ShowName(bool reset)
  {
    int i = 0, j = 0, maxcount = Math.Max(tablecount[0], tablecount[1]);
    if(reset){
      int k;
      for(k = 0; k < maxcount; k++){
        if(k < tablecount[0]) me.SetFXAttachment("table-m-" + k, null);
        if(k < tablecount[1]) me.SetFXAttachment("table-c-" + k, null);
      }
      return;
    }
    string sv_name = privateName;
    int[] tabl = new int[maxcount];
    int[] temp = new int[2];
    while(i < sv_name.size() and j < maxcount){
      tabl[j] = me.GetArabic(i, sv_name);
      if(tabl[j] < 0){
        if(i < sv_name.size() - 1){
          string part = sv_name[i, i + 2];
          tabl[j] = me.GetCirillic(part);
          if(tabl[j] >= 0) i++;
        }
        if(tabl[j] < 0){
          me.GetRome(i, sv_name, temp);
          if(temp[0] >= 0){
            tabl[j] = temp[0];
            i = i + temp[1];
          }else if(sv_name[i]==' ') tabl[j] = 21;
        }
      }
      j++;
      i++;
    }
    if(tablecount[1] > 0 and (tablecount[0] == 0 or (config & CFG_TABLECRADLE and (!(config & CFG_TABLEMACHT) or (j > tablecount[0] and j <= tablecount[1]))))){
      int count = Math.Min(tablecount[1], j);
      for(i = 0; i < count; i++){
        MeshObject tablM = me.AddTabl("table-c-" + i);
        tablM.SetFXTextureReplacement("table-texture", tex, tabl[count - i - 1]);
      }
    }else{
      int count = Math.Min(tablecount[0], j);
      for(i = 0; i < count; i++){
        MeshObject tablM = me.AddTabl("table-m-" + i);
        tablM.SetFXTextureReplacement("table-texture", tex, tabl[i]);
      }
    }
  }

  void SetNewGolPosition(bool enable)
  {
    inherited(enable);
    int code = Logic.SetStat(0, DEV_SIGNT, devices & DEV_SIGNT and signt != null);
    code = Logic.SetStat(code, DEV_ARROW, devices & DEV_ARROW and (kor_BU_1 or kor_BU_2));
    code = Logic.SetStat(code, DEV_POINTER, devices & DEV_POINTER and enable);
    string support = me.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedTag("support-" + code);
    if(support != ""){
      MeshObject suppobj = me.GetFXAttachment("support");
      if(suppobj) suppobj.SetMesh(support, 0.0);
    }
  }




  final string GetStringTablePlace(void)
  {
    Interface.Print(config + "  " + Logic.GetStat(config, CFG_TABLEAUTO));
    if(!Logic.GetStat(config, CFG_TABLEAUTO)){
      if(config & CFG_TABLEMACHT) return strTable.GetString("tableplace-macht");
      else if(config & CFG_TABLECRADLE) return strTable.GetString("tableplace-cradle");
    }
    return strTable.GetString("tableplace-auto");
  }

  string GetPropertyType(string propertyID)
  {
    if(propertyID == "along_displ") return "float,-1.0,1.0,0.1";
    return inherited(propertyID);
  }

  public string GetDescriptionHTML(void)
  {
    string ret = me.MakeTitle(me.GetAsset().GetStringTable().GetString("title")), izjname;
    int pncelltype = CELL_ERROR, sncelltype = CELL_ERROR, prcelltype = CELL_ERROR, i;
    if(privateName != "") pncelltype = CELL_VALUE;
    if(stationName != "") sncelltype = CELL_VALUE;
    if(ProtectGroup != "") prcelltype = CELL_VALUE;
    if(izojoin) izjname = izojoin.GetLocalisedName();
    tmpassets = null;

    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-general"), "colspan=4", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("private-name", strTable.GetString("name") + ":", strTable.GetString("tooltip-setname"), "width=180", CELL_KEY) +
                me.MakeLinkCell("private-name", privateName, strTable.GetString("tooltip-setname"), "colspan=3", pncelltype) + HTMLWindow.EndRow();
    if(!(Type & ST_PERMOPENED)){
      ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("station_name", strTable.GetString("station") + ":", strTable.GetString("tooltip-selectstation"), "width=180", CELL_KEY) +
                  me.MakeLinkCell("station_name", stationName, strTable.GetString("tooltip-selectstation"), "", sncelltype) + me.MakeLinkCell("station_create", me.GetIcon("icon-add"),
                  strTable.GetString("tooltip-addstation"), "width=16", CELL_KEY);
      if(stationName != "") ret = ret + me.MakeLinkCell("station_delete", me.GetIcon("icon-delete"), strTable.GetString("tooltip-deletestation"), "width=16", CELL_KEY) + HTMLWindow.EndRow();
      else  ret = ret + me.MakeCell(me.GetIcon("icon-delete-d"), "width=16", CELL_KEY) + HTMLWindow.EndRow();
    }
    if(tablecount[0] > 0 and tablecount[1] > 0){ //Если есть таблички на мачте и люльке
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("tableplace") + ":", "width=180", CELL_KEY) +
                  me.MakeLinkCell("place-table", me.GetStringTablePlace(), strTable.GetString("tooltip-tableplace"), "colspan=3", CELL_VALUE) + HTMLWindow.EndRow();
    }
    //Вставить смену расположения таблички предвходного светофора
    ret = ret + HTMLWindow.EndTable() + "<br />";

    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-linzs"), "colspan=3", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("lens_kit") + ":", "width=180", CELL_KEY) + me.MakeLinkCell("lens_kit", me.ConvLensKit(lens_kit),
                strTable.GetString("tooltip-lens_kit"), "", CELL_VALUE) + me.MakeLinkCell("lens_kit_ex", me.GetIcon("icon-edit"),
                strTable.GetString("tooltip-lens_kit_ex"), "width=16", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("abtype") + ":", "width=220", CELL_KEY) +
                me.MakeLinkCell("abtype", me.GetStringLockedType(), strTable.GetString("tooltip-abtype"), "colspan=2", CELL_VALUE) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable() + "<br />";

    if (ex_sgn[zxIndication.STATE_R] and ex_sgn[zxIndication.STATE_W]) {   // только у светофоров с красным и белым немигающим
      ret = ret + HTMLWindow.StartTable("border='0' width='400'");
      ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("prigl_enabled", STT.GetString("prigl_enabled"), strTable.GetString("tooltip-prigl_enabled"), prigl_enabled, "", CELL_KEY) + HTMLWindow.EndRow();
      ret = ret + HTMLWindow.EndTable() + "<br />";
    }

    if(Type & (ST_IN | ST_OUT | ST_ROUTER)){
      ret = ret + HTMLWindow.StartTable("border='0' width='400'");
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-pathcontrol"), "colspan=2", CELL_HEAD) + HTMLWindow.EndRow();
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("priority") + ":", "", CELL_KEY) +
                  me.MakeLinkCell("priority", def_path_priority, strTable.GetString("tooltip-priority"), "align=center", CELL_VALUE) + HTMLWindow.EndRow();
      ret = ret + HTMLWindow.EndTable() + "<br />";
    }

    if(isMacht and devices & (DEV_SIGNT | DEV_ARROW | DEV_POINTER)){
      bool signtenabled = ((!kor_BU_1 and !kor_BU_2) or devices & DEV_SIGNTANDARROW) and (!MU or devices & DEV_SIGNTANDPOINTER);
      bool arrowenabled = (!signt or devices & DEV_SIGNTANDARROW) and (!MU or devices & DEV_ARROWANDPOINTER);
      bool pointerenabled = (!signt or devices & DEV_SIGNTANDPOINTER) and ((!kor_BU_1 and !kor_BU_2) or devices & DEV_ARROWANDPOINTER);
      ret = ret + HTMLWindow.StartTable("border='0' width='400'");
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-devices"), "colspan=3", CELL_HEAD) + HTMLWindow.EndRow();
      if(devices & DEV_SIGNT and signtenabled) ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("signt", strTable.GetString("signt"), strTable.GetString("tooltip-signt"), signt != null, "colspan=3", CELL_KEY) + HTMLWindow.EndRow();
      else if(devices & DEV_SIGNT) ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkDisabledCell(strTable.GetString("signt"), signt != null, "colspan=3", CELL_KEY_DISABLED) + HTMLWindow.EndRow();
      if(devices & DEV_ARROW and arrowenabled) ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("kor_BU_2") + ":", "", CELL_KEY) + me.MakeRadioButtonLinkCell("kor_BU_2", strTable.GetString("arrowtwo"),
                                                           strTable.GetString("tooltip-arrow"), kor_BU_2, "", CELL_VALUE) + me.MakeRadioButtonLinkCell("kor_BU_1", strTable.GetString("arrowone"),
                                                           strTable.GetString("tooltip-arrow"), kor_BU_1, "", CELL_VALUE) + HTMLWindow.EndRow();
      else if(devices & DEV_ARROW) ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("kor_BU_2") + ":", "", CELL_KEY_DISABLED) + me.MakeRadioButtonLinkDisabledCell(strTable.GetString("arrowtwo"),
                                               kor_BU_2, "", CELL_KEY_DISABLED) + me.MakeRadioButtonLinkDisabledCell(strTable.GetString("arrowone"), kor_BU_1, "", CELL_KEY_DISABLED) + HTMLWindow.EndRow();
      if(devices & DEV_POINTER and pointerenabled) ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("MU", STT.GetString("MU"), strTable.GetString("tooltip-pointer"), MU != null, "colspan=3", CELL_KEY) + HTMLWindow.EndRow();
      else if(devices & DEV_POINTER) ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkDisabledCell(STT.GetString("MU"), MU != null, "colspan=3", CELL_KEY_DISABLED) + HTMLWindow.EndRow();
      if(MU){
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-pointer"), "colspan=3", CELL_HEAD) + HTMLWindow.EndRow();
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("color_rsign_desc") + ":", "", CELL_KEY) +
                    me.MakeLinkCell("color_rsign", STT.GetString("color_rsign_" + MU.textureName), strTable.GetString("tooltip-pointercolor"), "colspan=2", CELL_VALUE) + HTMLWindow.EndRow();
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("type_matrix") + ":", "", CELL_KEY) +
                    me.MakeLinkCell("type_matrix", STT.GetString("typematrix_" + MU.typeMatrix), strTable.GetString("tooltip-pointertype"), "colspan=2", CELL_VALUE) + HTMLWindow.EndRow();
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("twait") + ":", "", CELL_KEY) +
                    me.MakeLinkCell("twait", MU.timeToWait, strTable.GetString("tooltip-pointerdelay"), "colspan=2", CELL_VALUE) + HTMLWindow.EndRow();
      }
      ret = ret + HTMLWindow.EndTable() + "<br />";
    }

    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-cercuit"), "colspan=5", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeRadioButtonLinkCell("code_freq/0", STT.GetString("uncoded"), strTable.GetString("tooltip-freq"), !(code_freq & 7),"align=center", CELL_VALUE) +
                me.MakeRadioButtonLinkCell("code_freq/1", STT.GetString("ALS25"), strTable.GetString("tooltip-freq"), code_freq & 1,"align=center", CELL_VALUE) +
                me.MakeRadioButtonLinkCell("code_freq/2", STT.GetString("ALS50"), strTable.GetString("tooltip-freq"), code_freq & 2,"align=center", CELL_VALUE) +
                me.MakeRadioButtonLinkCell("code_freq/4", STT.GetString("ALS75"), strTable.GetString("tooltip-freq"), code_freq & 4,"align=center", CELL_VALUE) +
                me.MakeCheckBoxLinkCell("code_freq/8", STT.GetString("ALSEN"), strTable.GetString("tooltip-alsen"), code_freq & 8,"align=center", CELL_VALUE) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("yellow_code", STT.GetString("yellow_code"), strTable.GetString("tooltip-yellow_code"), yellow_code, "colspan=5", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable();
    if(Type & ST_IN){
      ret = ret + HTMLWindow.StartTable("border='0' width='400'");
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("code_dev") + ":", "", CELL_KEY) + me.MakeCheckBoxLinkCell("code_dev/1", STT.GetString("code_dev_t"),
                  strTable.GetString("tooltip-code_dev"), code_dev & 1 , "align=center", CELL_VALUE) + me.MakeCheckBoxLinkCell("code_dev/2", STT.GetString("code_dev_f"),
                  strTable.GetString("tooltip-code_dev"), code_dev & 2 , "align=center", CELL_VALUE) + HTMLWindow.EndRow();
      ret = ret + HTMLWindow.EndTable() + "<br />";
    }else if(Type & (ST_OUT | ST_ROUTER)){
      ret = ret + HTMLWindow.StartTable("border='0' width='400'");
      ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("code_dev/1", STT.GetString("code_dev"), strTable.GetString("tooltip-code_dev"), code_dev & 1, "", CELL_KEY) + HTMLWindow.EndRow();
      //ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("yellow_code", STT.GetString("yellow_code"), strTable.GetString("tooltip-yellow_code"), yellow_code, "", CELL_KEY) + HTMLWindow.EndRow();
      ret = ret + HTMLWindow.EndTable() + "<br />";
    }else ret = ret + "<br />";

    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-signaltype"), "", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("type/UNTYPED", STT.GetString("UNTYPED_flag"), strTable.GetString("tooltip-signaltype"), Type & ST_UNTYPED, "", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("type/IN", STT.GetString("IN_flag"), strTable.GetString("tooltip-signaltype"), Type & ST_IN, "", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("type/OUT", STT.GetString("OUT_flag"), strTable.GetString("tooltip-signaltype"), Type & ST_OUT, "", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("type/ROUTER", STT.GetString("ROUTER_flag"), strTable.GetString("tooltip-signaltype"), Type & ST_ROUTER, "", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("type/UNLINKED", STT.GetString("UNLINKED_flag"), strTable.GetString("tooltip-signaltype"), Type & ST_UNLINKED, "", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("type/PERMOPENED", STT.GetString("PERMOPENED_flag"), strTable.GetString("tooltip-signaltype"), Type & ST_PERMOPENED, "", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("type/SHUNT", STT.GetString("SHUNT_flag"), strTable.GetString("tooltip-signaltype"), Type & ST_SHUNT, "", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("type/ZAGRAD", STT.GetString("ZAGRAD_flag"), strTable.GetString("tooltip-signaltype"), Type & ST_PROTECT, "", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable() + "<br />";

    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-conduct"), "colspan=2", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("pause_bef_red") + ":", "width=220", CELL_KEY) +
                me.MakeLinkCell("pause_bef_red", me.GetCleanFloatString(pause_bef_red), strTable.GetString("tooltip-pause_bef_red"), "", CELL_VALUE) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable() + "<br />";


    if(Type & ST_IN){
      ret = ret + HTMLWindow.StartTable("border='0' width='400'");
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-span"), "colspan=4", CELL_HEAD) + HTMLWindow.EndRow();
      ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("make_span", STT.GetString("make_span"), strTable.GetString("tooltip-make_span"), "colspan=4 align=center", CELL_KEY) + HTMLWindow.EndRow();
		  if(span_soup and span_soup.GetNamedTagAsBool("Inited")){
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("spandirect") + ":", "width=80", CELL_KEY);
        if(wrong_dir){
          ret = ret + me.MakeLinkCell("spanTrackFromMe", privateName + "@" + stationName, strTable.GetString("tooltip-switch_span"), "align=center", CELL_VALUE) + me.MakeCell("&gt;&gt;&gt;",
                      "align=center width=22", CELL_KEY) + me.MakeCell(span_soup.GetNamedTag("end_sign_n") + "@" + span_soup.GetNamedTag("end_sign_s"), "align=center", CELL_VALUE);
        }else{
          ret = ret + me.MakeCell(privateName + "@" + stationName, "align=center", CELL_VALUE) + me.MakeCell("&lt;&lt;&lt;", "align=center width=22", CELL_KEY) + me.MakeLinkCell("spanTrackFromOther",
                      span_soup.GetNamedTag("end_sign_n") + "@" + span_soup.GetNamedTag("end_sign_s"), strTable.GetString("tooltip-switch_span"), "align=center", CELL_VALUE);
        }
        ret = ret + HTMLWindow.EndRow();
      }
      ret = ret + HTMLWindow.EndTable() + "<br />";
    }

    if(Type & ST_PROTECT){
      ret = ret + HTMLWindow.StartTable("border='0' width='400'");
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-protect"), "colspan=4", CELL_HEAD) + HTMLWindow.EndRow();
      ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("protect_name", STT.GetString("protect_name") + ":", strTable.GetString("tooltip-selectprotect"), "width=180", CELL_KEY) +
                  me.MakeLinkCell("protect_name", ProtectGroup, strTable.GetString("tooltip-selectprotect"), "", prcelltype) + me.MakeLinkCell("protect_create", me.GetIcon("icon-add"),
                  strTable.GetString("tooltip-createprotect"), "width=16", CELL_KEY);
      if(ProtectGroup != "") ret = ret + me.MakeLinkCell("protect_delete", me.GetIcon("icon-delete"), strTable.GetString("tooltip-deleteprotect"), "width=16", CELL_KEY) + HTMLWindow.EndRow();
      else  ret = ret + me.MakeCell(me.GetIcon("icon-delete-d"), "width=16", CELL_KEY) + HTMLWindow.EndRow();
      ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("protect_influence", STT.GetString("protect_influence"), strTable.GetString("tooltip-protectinfluence"), protect_influence, "colspan=4", CELL_KEY) + HTMLWindow.EndRow();
      int count = protect_soup.GetNamedTagAsInt("number");
      for(i = 0; i < count; i++){
		    zxSignal sign = cast<zxSignal>(Router.GetGameObject(protect_soup.GetNamedTag(i)));
        if(sign) ret = ret + HTMLWindow.StartRow() + me.MakeCell(sign.privateName, "colspan=3", CELL_KEY) + me.MakeLinkCell("remove_protect_sign/" + protect_soup.GetNamedTag(i),
                             me.GetIcon("icon-delete"), strTable.GetString("tooltip-removesignfromprotect"), "width=16", CELL_KEY) + HTMLWindow.EndRow();
      }



      ret = ret + HTMLWindow.EndTable() + "<br />";
    }









    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-offset"), "colspan=6", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("displace") + ":", "colspan=5", CELL_KEY) +
                me.MakeLinkCell("displace", me.GetCleanFloatString(displacement), strTable.GetString("tooltip-offsetaxis"), "align=center", CELL_VALUE) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("displace1/-3.20", "-3.20", strTable.GetString("tooltip-offsetAxis-defleft"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/-2.65", "-2.65", strTable.GetString("tooltip-offsetAxis-defleft"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/-2.50", "-2.50", strTable.GetString("tooltip-offsetAxis-defleft"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/2.50", "2.50", strTable.GetString("tooltip-offsetAxis-defright"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/2.65", "2.65", strTable.GetString("tooltip-offsetAxis-defright"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/3.20", "3.20", strTable.GetString("tooltip-offsetAxis-defright"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("offsetalong") + ":", "colspan=4", CELL_KEY) +
                me.MakeLinkCell("along_displ", me.GetCleanFloatString(along_displ), strTable.GetString("tooltip-offsetalong"), "align=center", CELL_VALUE) +
                me.MakeLinkCell("offsetalongdef", "0.00", strTable.GetString("tooltip-offsetalongdef"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("vert_displ") + ":", "colspan=3", CELL_KEY) +
                me.MakeLinkCell("vert_displ", me.GetCleanFloatString(vert_displ), strTable.GetString("tooltip-offsetHeight"), "align=center", CELL_VALUE) +
                me.MakeLinkCell("offsetHeightlow", "-0.20", strTable.GetString("tooltip-offsetHeightlow"), "align=center", CELL_KEY) +
                me.MakeLinkCell("offsetHeightdef", "0.00", strTable.GetString("tooltip-offsetHeightdef"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable() + "<br />";


    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-outside"), "colspan=3", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("izojoin", strTable.GetString("izojoin") + ":", strTable.GetString("tooltip-selectizojoin"), "width=220", CELL_KEY) +
                me.MakeLinkCell("izojoin", izjname, strTable.GetString("tooltip-selectizojoin"), "", CELL_VALUE);
    if(izojoin) ret = ret + me.MakeLinkCell("deleteizojoin", me.GetIcon("icon-delete"), strTable.GetString("tooltip-deleteizojoin"), "width=16", CELL_VALUE) + HTMLWindow.EndRow();
    else ret = ret + me.MakeCell(me.GetIcon("icon-delete-d"), "width=16", CELL_VALUE) + HTMLWindow.EndRow();
    if(izojoin){
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("reverse-izojoin") + ":", "width=220", CELL_KEY) +
                  me.MakeLinkCell("izojoinreverse", me.GetStringBool(izojoinreverse), strTable.GetString("tooltip-reverse-izojoin"), "colspan=2", CELL_VALUE) + HTMLWindow.EndRow();
    }
    ret = ret + HTMLWindow.EndTable() + "<br />";

    if(!( Type & ST_UNLINKED)){
      ret = ret + HTMLWindow.StartTable("border='0' width='400'");
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-speedboard"), "colspan=3", CELL_HEAD) + HTMLWindow.EndRow();
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("signalcode"), "align=center", CELL_KEY) +
                  me.MakeCell(strTable.GetString("passenger"), "align=center", CELL_KEY) +
                  me.MakeCell(strTable.GetString("cargo"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
      for(i = 0; i < 26; i++){
        if(ex_sgn[i] and (i != 9 or ab4) and i != 19){
          ret = ret + HTMLWindow.StartRow() + me.MakeCell(me.TranslNames(LC.sgn_st[i].l.name), "", CELL_KEY) +
                      me.MakeLinkCell("speed/p/" + i, speed_soup.GetNamedTagAsInt("p" + i), strTable.GetString("tooltip-speedboard"), "align=center", CELL_VALUE) +
                      me.MakeLinkCell("speed/c/" + i, speed_soup.GetNamedTagAsInt("c" + i), strTable.GetString("tooltip-speedboard"), "align=center", CELL_VALUE) + HTMLWindow.EndRow();
        }
      }
      ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("speed_init", strTable.GetString("setdefault"), strTable.GetString("tooltip-speedboard-default"), "align=center", CELL_KEY) +
                  me.MakeLinkCell("speed_copy", strTable.GetString("copy"), strTable.GetString("tooltip-speedboard-copy"), "align=center", CELL_KEY) +
                  me.MakeLinkCell("speed_paste", strTable.GetString("past"), strTable.GetString("tooltip-speedboard-past"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
      ret = ret + HTMLWindow.EndTable() + "<br />";
    }

    return ret + "<br /><br />";
  }


  public void LinkPropertyValue(string id)
  {
    if(id == "place-table"){
      int set = (int)(config & CFG_TABLEAUTO) + 1;
      if(set > 3) set = 1;
      config = Logic.SetStat(config, CFG_TABLEMACHT, set & CFG_TABLEMACHT);
      config = Logic.SetStat(config, CFG_TABLECRADLE, set & CFG_TABLECRADLE);
      me.ShowName(true);
    }else if(id == "type_matrix"){
		  if(MU.typeMatrix == "9") MU.typeMatrix="19";
		  else if(MU.typeMatrix=="19") MU.typeMatrix="x";
		  else MU.typeMatrix="9";
		  MU.SetTypeMatrix();

    }else inherited(id);
  }






	public Soup GetProperties(void)
	{
		Soup soup = inherited();
    soup.SetNamedTag("cradle-config", config);
    return soup;
  }

  public void SetProperties(Soup soup)
  {
    config = soup.GetNamedTagAsInt("cradle-config");
    along_displ = soup.GetNamedTagAsFloat("offsetalong", along_displ);
    inherited(soup);
  }

  public void Init(Asset myAsset)
  {
    Soup soup = myAsset.GetConfigSoup().GetNamedSoup("extensions");

    string[] argtablecount = Str.Tokens(soup.GetNamedTag("tablescount"), ",");
    if(argtablecount.size() != 2){
      Interface.Exception("Signal parameter 'tablescount' has wrong value or missing");
      return;
    }
    tablecount[0] = Str.ToInt(argtablecount[0]);
    tablecount[1] = Str.ToInt(argtablecount[1]);
    config = Logic.SetStat(config, CFG_TABLEMACHT, tablecount[0] > 0);
    config = Logic.SetStat(config, CFG_TABLECRADLE, tablecount[1] > 0);


    inherited(myAsset);
  }

};