include "multiplayersessionmanager.gs"
include "zx_extension_library.gs"
include "interfaceprovider.gs"
include "scenariobehavior.gs"

class ZX_ALSNControlPoint_Console isclass ScenarioBehavior, InterfaceProviderBase
{
  ZX_Extension_Library library; //Основная библиотека
  //ZX_ALSNControlPoint controlPoint; //Контрольный пункт, которым выполняется управление
  OAChat oachat;
  int pointIndex = -1; //Выбраный контрольный пункт

  void OnUpdateBrowser(void){}
  void OnCreateBrowser(Message msg){}
  void OnCloseBrowser(Message msg){}


  //Получение имени пользователя из под которого запущена игра
  public final string GetCurrentUserName()
  {
    string userName = oachat.GetLocalUserProfileName();
    Str.ToLower(userName);
    return userName;
  }

  final bool AvailableControl(int index){return true;}


  final void SetFrequency(int index, int frequency, bool sync)
  {
    if(sync or !MultiplayerGame.IsActive() or MultiplayerGame.IsServer()){
      ZX_ALSNControlPoint controlPoint = library.GerALSNPoint(index);
      if(controlPoint){
        controlPoint.SetFrequency(frequency);
        if(!sync and MultiplayerGame.IsActive()){
          Soup soup = Constructors.NewSoup();
          soup.SetNamedTag("Object", "Frequency");
          soup.SetNamedTag("Index", index);
          soup.SetNamedTag("Frequency", frequency);
          MultiplayerGame.BroadcastGameplayMessage("ZXALSNCONTROLPOINT_Sync", "SyncObject", soup);
        }
      }
    }else if(me.AvailableControl(index)){
      Soup soup = Constructors.NewSoup();
      soup.SetNamedTag("Action", "SetFrequency");
      soup.SetNamedTag("Index", index);
      soup.SetNamedTag("Frequency", frequency);
      MultiplayerGame.SendGameplayMessageToServer("ZXALSNCONTROLPOINT_Sync", "CommandQuery", soup);
    }
  }

  final void SetCode(int index, int code, bool sync)
  {
    if(sync or !MultiplayerGame.IsActive() or MultiplayerGame.IsServer()){
      ZX_ALSNControlPoint controlPoint = library.GerALSNPoint(index);
      if(controlPoint){
        controlPoint.SetCode(code);
        if(!sync and MultiplayerGame.IsActive()){
          Soup soup = Constructors.NewSoup();
          soup.SetNamedTag("Object", "Сode");
          soup.SetNamedTag("Index", index);
          soup.SetNamedTag("Code", code);
          MultiplayerGame.BroadcastGameplayMessage("ZXALSNCONTROLPOINT_Sync", "SyncObject", soup);
        }
      }
    }else if(me.AvailableControl(index)){
      Soup soup = Constructors.NewSoup();
      soup.SetNamedTag("Action", "SetCode");
      soup.SetNamedTag("Index", index);
      soup.SetNamedTag("Code", code);
      MultiplayerGame.SendGameplayMessageToServer("ZXALSNCONTROLPOINT_Sync", "CommandQuery", soup);
    }
  }

  final void CommandQueryHandler(Message msg)
  {
    if(MultiplayerGame.IsActive() and MultiplayerGame.IsServer()){
      Soup soup = cast<Soup>msg.paramSoup;
      string Action = soup.GetNamedTag("Action");

      if(Action == "SetFrequency"){ //Если запрос установки частоты кодирования
        int index = soup.GetNamedTagAsInt("Index", -1);
        int frequency = soup.GetNamedTagAsInt("Frequency", ALSN_Provider.FREQ_NONE);
        if(index >= 0) me.SetFrequency(index, frequency, false);
      }else if(Action == "SetCode"){ //Если запрос установки кода сигнала
        int index = soup.GetNamedTagAsInt("Index", -1);
        int code = soup.GetNamedTagAsInt("Code", ALSN_Provider.CODE_NONE);
        if(index >= 0) me.SetCode(index, code, false);
      }
    }
  }

  //Обработка синхранизации объектов
  final void SyncObjectHandler(Message msg)
  {
    if(MultiplayerGame.IsActive() and MultiplayerGame.IsClient()){
      Soup soup = cast<Soup>msg.paramSoup;
      string Object = soup.GetNamedTag("Object");

      if(Object == "Frequency"){
        int index = soup.GetNamedTagAsInt("Index", -1);
        int frequency = soup.GetNamedTagAsInt("Frequency", ALSN_Provider.FREQ_NONE);
        if(index >= 0) me.SetFrequency(index, frequency, true);
      }else if(Object == "Сode"){ //Если запрос установки кода сигнала
        int index = soup.GetNamedTagAsInt("Index", -1);
        int code = soup.GetNamedTagAsInt("Code", ALSN_Provider.CODE_NONE);
        if(index >= 0) me.SetCode(index, code, true);
      }
    }
  }

  final void UpdatePointHandler(Message msg)
  {
    if(pointIndex >= 0){
      ZX_ALSNControlPoint controlPoint = library.GerALSNPoint(pointIndex);
      if(controlPoint == msg.src) me.OnUpdateBrowser();
    }
  }



  public Soup GetProperties(void)
  {
    Soup soup = inherited();
    if(!library.GetALSNPointConsole()) library.SetALSNPointConsole(me);
    soup.SetNamedTag("Init", (library.GetALSNPointConsole() == me));
    return soup;
  }

  public void SetProperties(Soup soup)
  {
    inherited(soup);
    if(soup.GetNamedTagAsBool("Init"))
      library.SetALSNPointConsole(me);
  }


  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    me.AddHandler(me, "ZXALSNCONTROLPOINT_Sync", "CommandQuery", "CommandQueryHandler");
    me.AddHandler(me, "ZXALSNCONTROLPOINT_Sync", "SyncObject", "SyncObjectHandler");
    me.AddHandler(me, "ZXALSNCONTROLPOINT", "Update", "UpdatePointHandler");
		me.AddHandler(me, "ZXALSNCONTROLPOINT", "ConsoleOpen", "OnCreateBrowser");

    me.AddHandler(me, "Browser-URL", "", "BrowserURLHandler");
    me.AddHandler(me, "Browser-Closed", "", "OnCloseBrowser");



    library = cast<ZX_Extension_Library>TrainzScript.GetLibrary(myAsset.LookupKUIDTable("extension-core-library"));
    strTable = myAsset.GetStringTable(); //library.GetAsset().GetStringTable();

    if(!library.GetALSNPointConsole())library.SetALSNPointConsole(me);
  }

};