include "zx_routerextension.gs"

class zxHangerRouter isclass zxRouterExtension
{

  define int CFG_TABLEMACHT       = 1; //Таблички крепяться к мачте
  define int CFG_TABLECRADLE      = 2; //Таблички крепяться к люльке
  define int CFG_TABLEAUTO        = CFG_TABLEMACHT | CFG_TABLECRADLE; //Автоматический выбор расположения табличек

  int[] tablecount = new int[2]; //Количество табличек: 0 - на мачте, 1 - на люльке

  int config = 0; //Тут будут режать настройки и состояние светофора
  float offsetalong = 0.0;

  void UpdateSignalPosition(void)
  {
    float height = me.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedTagAsFloat("cradle-height");
    me.SetMeshTranslation("default", displacement, offsetalong, height + vert_displ);
  }

  MeshObject AddTabl(string effect)
  {
    MeshObject ret = me.GetFXAttachment(effect);
    if(!ret) ret = me.SetFXAttachment(effect, tabl_m);
    return ret;
  }

  public void ShowName(bool reset)
  {
    int i = 0, j = 0, maxcount = Math.Max(tablecount[0], tablecount[1]);
    if(reset){
      int k;
      for(k = 0; k < maxcount; k++){
        if(k < tablecount[0]) me.SetFXAttachment("table-m-" + k, null);
        if(k < tablecount[1]) me.SetFXAttachment("table-c-" + k, null);
      }
      return;
    }
    string sv_name = privateName;
    int[] tabl = new int[maxcount];
    int[] temp = new int[2];
    while(i < sv_name.size() and j < maxcount){
      tabl[j] = me.GetArabic(i, sv_name);
      if(tabl[j] < 0){
        if(i < sv_name.size() - 1){
          string part = sv_name[i, i + 2];
          tabl[j] = me.GetCirillic(part);
          if(tabl[j] >= 0) i++;
        }
        if(tabl[j] < 0){
          me.GetRome(i, sv_name, temp);
          if(temp[0] >= 0){
            tabl[j] = temp[0];
            i = i + temp[1];
          }else if(sv_name[i]==' ') tabl[j] = 21;
        }
      }
      j++;
      i++;
    }
    if(tablecount[1] > 0 and (tablecount[0] == 0 or (config & CFG_TABLECRADLE and (!(config & CFG_TABLEMACHT) or (j > tablecount[0] and j <= tablecount[1]))))){
      int count = Math.Min(tablecount[1], j);
      for(i = 0; i < count; i++){
        MeshObject tablM = me.AddTabl("table-c-" + i);
        tablM.SetFXTextureReplacement("table-texture", tex, tabl[count - i - 1]);
      }
    }else{
      int count = Math.Min(tablecount[0], j);
      for(i = 0; i < count; i++){
        MeshObject tablM = me.AddTabl("table-m-" + i);
        tablM.SetFXTextureReplacement("table-texture", tex, tabl[i]);
      }
    }
  }

  final string GetStringTablePlace(void)
  {
    Interface.Print(config + "  " + Logic.GetStat(config, CFG_TABLEAUTO));
    if(!Logic.GetStat(config, CFG_TABLEAUTO)){
      if(config & CFG_TABLEMACHT) return strTable.GetString("tableplace-macht");
      else if(config & CFG_TABLECRADLE) return strTable.GetString("tableplace-cradle");
    }
    return strTable.GetString("tableplace-auto");
  }

  string GetPropertyType(string propertyID)
  {
    if(propertyID == "offsetalong") return "float,-1.0,1.0";
    return inherited(propertyID);
  }

  string GetPropertyName(string propertyID)
  {
    if(propertyID == "offsetalong") return strTable.GetString("interface-offsetalong");
    return inherited(propertyID);
  }

  public string GetPropertyValue(string propertyID)
  {
    if(propertyID == "offsetalong") return (string)offsetalong;
    return inherited(propertyID);
  }

  public string GetDescriptionHTML(void)
  {
    string ret = me.MakeTitle(me.GetAsset().GetStringTable().GetString("title")), izjname;
    int pncelltype = CELL_ERROR;
    if(privateName != "") pncelltype = CELL_VALUE;
    if(izojoin) izjname = izojoin.GetLocalisedName();
    tmpassets = null;


    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-general"), "colspan=3", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("private-name", strTable.GetString("name") + ":", strTable.GetString("tooltip-setroutename"), "width=160", CELL_KEY) +
                me.MakeLinkCell("private-name", privateName, strTable.GetString("tooltip-setroutename"), "colspan=2", pncelltype) + HTMLWindow.EndRow();
    if(tablecount[0] > 0 and tablecount[1] > 0){ //Если есть таблички на мачте и люльке
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("tableplace") + ":", "width=160", CELL_KEY) +
                  me.MakeLinkCell("place-table", me.GetStringTablePlace(), strTable.GetString("tooltip-tableplace"), "colspan=2", CELL_VALUE) + HTMLWindow.EndRow();
    }
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("link_to", strTable.GetString("linktosignal") + ":", strTable.GetString("tooltip-linktosignal"), "width=160", CELL_KEY) +
                me.MakeLinkCell("link_to", me.GetAttachedSignalName(), strTable.GetString("tooltip-linktosignal"), "", CELL_VALUE);
    if(RouterO.OwnerSignal) ret = ret + me.MakeLinkCell("unlink_to", me.GetIcon("icon-delete"), strTable.GetString("tooltip-unlinktosignal"), "width=16", CELL_KEY) + HTMLWindow.EndRow();
    else  ret = ret + me.MakeCell(me.GetIcon("icon-delete-d"), "width=16", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable() + "<br />";

    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-pointer"), "colspan=2", CELL_HEAD) + HTMLWindow.EndRow();
    if(!RouterO.OwnerSignal){
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("linksignaldirection") + ":", "width=160", CELL_KEY) +
                  me.MakeLinkCell("control_dir", me.GetAttachedSignalDirection(), strTable.GetString("tooltip-linksignaldirection"), "", CELL_VALUE) + HTMLWindow.EndRow();
    }
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(ST.GetString("color_rsign_desc") + ":", "width=160", CELL_KEY) +
                me.MakeLinkCell("color_rsign", ST.GetString("color_rsign_" + RouterO.textureName), strTable.GetString("tooltip-pointercolor"), "", CELL_VALUE) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(ST.GetString("type_matrix") + ":", "width=160", CELL_KEY) +
                me.MakeLinkCell("type_matrix", ST.GetString("typematrix_" + RouterO.typeMatrix), strTable.GetString("tooltip-pointertype"), "", CELL_VALUE) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(ST.GetString("twait") + ":", "width=160", CELL_KEY) +
                me.MakeLinkCell("twait", RouterO.timeToWait, strTable.GetString("tooltip-pointerdelay"), "", CELL_VALUE) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable() + "<br />";


    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-offset"), "colspan=6", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(ST.GetString("displace") + ":", "colspan=5", CELL_KEY) +
                me.MakeLinkCell("displace", me.GetCleanFloatString(displacement), strTable.GetString("tooltip-offsetaxis"), "align=center", CELL_VALUE) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("displace1/-3.20", "-3.20", strTable.GetString("tooltip-offsetAxis-defleft"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/-2.65", "-2.65", strTable.GetString("tooltip-offsetAxis-defleft"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/-2.50", "-2.50", strTable.GetString("tooltip-offsetAxis-defleft"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/2.50", "2.50", strTable.GetString("tooltip-offsetAxis-defright"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/2.65", "2.65", strTable.GetString("tooltip-offsetAxis-defright"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/3.20", "3.20", strTable.GetString("tooltip-offsetAxis-defright"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("offsetalong") + ":", "colspan=4", CELL_KEY) +
                me.MakeLinkCell("offsetalong", me.GetCleanFloatString(offsetalong), strTable.GetString("tooltip-offsetalong"), "align=center", CELL_VALUE) +
                me.MakeLinkCell("offsetalongdef", "0.00", strTable.GetString("tooltip-offsetalongdef"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(ST.GetString("vert_displ") + ":", "colspan=3", CELL_KEY) +
                me.MakeLinkCell("vert_displ", me.GetCleanFloatString(vert_displ), strTable.GetString("tooltip-offsetHeight"), "align=center", CELL_VALUE) +
                me.MakeLinkCell("offsetHeightlow", "-0.20", strTable.GetString("tooltip-offsetHeightlow"), "align=center", CELL_KEY) +
                me.MakeLinkCell("offsetHeightdef", "0.00", strTable.GetString("tooltip-offsetHeightdef"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable() + "<br />";

    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-outside"), "colspan=3", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("izojoin", strTable.GetString("izojoin") + ":", strTable.GetString("tooltip-selectizojoin"), "width=220", CELL_KEY) +
                me.MakeLinkCell("izojoin", izjname, strTable.GetString("tooltip-selectizojoin"), "", CELL_VALUE);
    if(izojoin) ret = ret + me.MakeLinkCell("deleteizojoin", me.GetIcon("icon-delete"), strTable.GetString("tooltip-deleteizojoin"), "width=16", CELL_VALUE) + HTMLWindow.EndRow();
    else ret = ret + me.MakeCell(me.GetIcon("icon-delete-d"), "width=16", CELL_VALUE) + HTMLWindow.EndRow();
    if(izojoin){
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("reverse-izojoin") + ":", "width=220", CELL_KEY) +
                  me.MakeLinkCell("izojoinreverse", me.GetStringBool(izojoinreverse), strTable.GetString("tooltip-reverse-izojoin"), "colspan=2", CELL_VALUE) + HTMLWindow.EndRow();
    }
    ret = ret + HTMLWindow.EndTable() + "<br />";

    return ret + "<br /><br />";
  }

  public void LinkPropertyValue(string id)
  {
    if(id == "place-table"){
      int set = (int)(config & CFG_TABLEAUTO) + 1;
      if(set > 3) set = 1;
      config = Logic.SetStat(config, CFG_TABLEMACHT, set & CFG_TABLEMACHT);
      config = Logic.SetStat(config, CFG_TABLECRADLE, set & CFG_TABLECRADLE);
      me.ShowName(true);
    }else if(id == "type_matrix"){
		  if(RouterO.typeMatrix == "9") RouterO.typeMatrix="19";
		  else if(RouterO.typeMatrix=="19") RouterO.typeMatrix="x";
		  else RouterO.typeMatrix="9";
		  RouterO.SetTypeMatrix();
    }else if(id == "offsetalongdef"){
      offsetalong = 0.0;
      me.UpdateSignalPosition();
    }else inherited(id);
  }

  void SetPropertyValue(string id, float val)
  {
    if(id == "offsetalong"){
      offsetalong = val;
      me.UpdateSignalPosition();
    }else inherited(id, val);
  }




	public Soup GetProperties(void)
	{
		Soup soup = inherited();
    soup.SetNamedTag("cradle-config", config);
    soup.SetNamedTag("offsetalong", offsetalong);
    return soup;
  }

  public void SetProperties(Soup soup)
  {
    config = soup.GetNamedTagAsInt("cradle-config");
    offsetalong = soup.GetNamedTagAsFloat("offsetalong");
    inherited(soup);
  }


  public void Init(Asset myAsset)
  {
    Soup soup = myAsset.GetConfigSoup().GetNamedSoup("extensions");

    string[] argtablecount = Str.Tokens(soup.GetNamedTag("tablescount"), ",");
    if(argtablecount.size() != 2){
      Interface.Exception("Signal parameter 'tablescount' has wrong value or missing");
      return;
    }
    tablecount[0] = Str.ToInt(argtablecount[0]);
    tablecount[1] = Str.ToInt(argtablecount[1]);
    config = Logic.SetStat(config, CFG_TABLEMACHT, tablecount[0] > 0);
    config = Logic.SetStat(config, CFG_TABLECRADLE, tablecount[1] > 0);


    inherited(myAsset);
  }


};