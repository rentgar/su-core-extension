include "zx_signalextension.gs"


class zxSignal_also isclass zxSignalExtension
{
  //Asset tableMesh;

  MeshObject AddTabl(int index, bool once)
  {
    MeshObject ret = SetFXAttachment("tabl" + index, tabl_m);
    //if(index == 0 and once) ret.SetMeshTranslation("default", 0.0, 0.0, 0.0);
    if(index == 0 and once) ret.SetMeshTranslation("default", 0.0, 0.0, 0.03);
    else if(index == 1) ret.SetMeshTranslation("default", 0.0, 0.0, -0.1);
    return ret;
  }

  public void ShowName(bool reset)
  {
    if(reset){
      me.SetFXAttachment("tabl0", null);
      me.SetFXAttachment("tabl1", null);
      return;
    }

    string sv_name = privateName;
    int[] tabl = new int[7];
    int[] temp = new int[2];
    int i = 0, j = 0;

    if(privateName != ""){
      string[] tok=Str.Tokens(privateName,"-");
      sv_name = tok[0];
    }

    while(i < sv_name.size()){
      tabl[j] = GetArabic(i, sv_name);
      if(tabl[j] < 0){
        if(i < sv_name.size() - 1){
          string part = sv_name[i, i + 2];
          tabl[j] = GetCirillic(part);
          if(tabl[j] >= 0) i++;
        }
        if(tabl[j] < 0){
          GetRome(i, sv_name, temp);
          if(temp[0] >= 0){
            tabl[j] = temp[0];
            i = i + temp[1];
          }else if(sv_name[i]==' ') tabl[j] = 21;
        }
      }
      j++;
      i++;
    }
    for(i = 0; i < 2; i++){
      if(i < j){
        MeshObject tablM = me.AddTabl(i, j > 1);
        tablM.SetFXTextureReplacement("table-texture", tex, tabl[i]);
      }
    }
	}

  public int GetALSNTypeSignal(void){return TYPE_CIRCUIT;}

  public string GetDescriptionHTML(void)
  {
    string ret = me.MakeTitle(strTable.GetString("title-also")), izjname;
    int celltype = CELL_ERROR, i;
    if(privateName != "") celltype = CELL_VALUE;
    if(izojoin) izjname = izojoin.GetLocalisedName();
    tmpassets = null;



    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-general"), "colspan=2", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("private-name", strTable.GetString("name") + ":", strTable.GetString("tooltip-setname"), "width='220'", CELL_KEY) +
                me.MakeLinkCell("private-name", privateName, strTable.GetString("tooltip-setname"), "", celltype) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("abtype") + ":", "width='220'", CELL_KEY) +
                me.MakeLinkCell("abtype", me.GetStringLockedType(), strTable.GetString("tooltip-abtype"), "", CELL_VALUE) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable() + "<br />";

    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-cercuit"), "colspan=5", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeRadioButtonLinkCell("code_freq/0", STT.GetString("uncoded"), strTable.GetString("tooltip-freq"), !(code_freq & 7),"align=center", CELL_VALUE) +
                me.MakeRadioButtonLinkCell("code_freq/1", STT.GetString("ALS25"), strTable.GetString("tooltip-freq"), code_freq & 1,"align=center", CELL_VALUE) +
                me.MakeRadioButtonLinkCell("code_freq/2", STT.GetString("ALS50"), strTable.GetString("tooltip-freq"), code_freq & 2,"align=center", CELL_VALUE) +
                me.MakeRadioButtonLinkCell("code_freq/4", STT.GetString("ALS75"), strTable.GetString("tooltip-freq"), code_freq & 4,"align=center", CELL_VALUE) +
                me.MakeCheckBoxLinkCell("code_freq/8", STT.GetString("ALSEN"), strTable.GetString("tooltip-alsen"), code_freq & 8,"align=center", CELL_VALUE) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("yellow_code", STT.GetString("yellow_code"), strTable.GetString("tooltip-yellow_code"), yellow_code, "colspan=5", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable() + "<br />";

    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-offset"), "colspan=6", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("displace") + ":", "colspan=5", CELL_KEY) +
                me.MakeLinkCell("displace", me.GetCleanFloatString(displacement), strTable.GetString("tooltip-offsetaxis"), "align=center", CELL_VALUE) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("displace1/-3.20", "-3.20", strTable.GetString("tooltip-offsetAxis-defleft"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/-2.65", "-2.65", strTable.GetString("tooltip-offsetAxis-defleft"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/-2.50", "-2.50", strTable.GetString("tooltip-offsetAxis-defleft"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/2.50", "2.50", strTable.GetString("tooltip-offsetAxis-defright"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/2.65", "2.65", strTable.GetString("tooltip-offsetAxis-defright"), "align=center", CELL_KEY) +
                me.MakeLinkCell("displace1/3.20", "3.20", strTable.GetString("tooltip-offsetAxis-defright"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(STT.GetString("vert_displ") + ":", "colspan=3", CELL_KEY) +
                me.MakeLinkCell("vert_displ", me.GetCleanFloatString(vert_displ), strTable.GetString("tooltip-offsetHeight"), "align=center", CELL_VALUE) +
                me.MakeLinkCell("offsetHeightlow", "-0.20", strTable.GetString("tooltip-offsetHeightlow"), "align=center", CELL_KEY) +
                me.MakeLinkCell("offsetHeightdef", "0.00", strTable.GetString("tooltip-offsetHeightdef"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable() + "<br />";

    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-outside"), "colspan=3", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("izojoin", strTable.GetString("izojoin") + ":", strTable.GetString("tooltip-selectizojoin"), "width=220", CELL_KEY) +
                me.MakeLinkCell("izojoin", izjname, strTable.GetString("tooltip-selectizojoin"), "", CELL_VALUE);
    if(izojoin) ret = ret + me.MakeLinkCell("deleteizojoin", me.GetIcon("icon-delete"), strTable.GetString("tooltip-deleteizojoin"), "width=16", CELL_VALUE) + HTMLWindow.EndRow();
    else ret = ret + me.MakeCell(me.GetIcon("icon-delete-d"), "width=16", CELL_VALUE) + HTMLWindow.EndRow();
    if(izojoin){
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("reverse-izojoin") + ":", "width=220", CELL_KEY) +
                  me.MakeLinkCell("izojoinreverse", me.GetStringBool(izojoinreverse), strTable.GetString("tooltip-reverse-izojoin"), "colspan=2", CELL_VALUE) + HTMLWindow.EndRow();
    }
    ret = ret + HTMLWindow.EndTable() + "<br />";


    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-speedboard"), "colspan=3", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("signalcode"), "align=center", CELL_KEY) +
                me.MakeCell(strTable.GetString("passenger"), "align=center", CELL_KEY) +
                me.MakeCell(strTable.GetString("cargo"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    for(i = 0; i < 26; i++){
      if(ex_sgn[i] and (i != 9 or ab4) and i != 19){
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(me.TranslNames(LC.sgn_st[i].l.name), "", CELL_KEY) +
                    me.MakeLinkCell("speed/p/" + i, speed_soup.GetNamedTagAsInt("p" + i), strTable.GetString("tooltip-speedboard"), "align=center", CELL_VALUE) +
                    me.MakeLinkCell("speed/c/" + i, speed_soup.GetNamedTagAsInt("c" + i), strTable.GetString("tooltip-speedboard"), "align=center", CELL_VALUE) + HTMLWindow.EndRow();
      }
    }
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("speed_init", strTable.GetString("setdefault"), strTable.GetString("tooltip-speedboard-default"), "align=center", CELL_KEY) +
                me.MakeLinkCell("speed_copy", strTable.GetString("copy"), strTable.GetString("tooltip-speedboard-copy"), "align=center", CELL_KEY) +
                me.MakeLinkCell("speed_paste", strTable.GetString("past"), strTable.GetString("tooltip-speedboard-past"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable() + "<br />";


    return ret;
  }


  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    //tableMesh = myAsset.FindAsset("tabl-mesh");
  }

};
