include "interfaceprovider.gs"
include "zx_router.gs"
include "logic.gs"

class zxRouterExtension isclass zxRouterBase, InterfaceProviderBase
{
  Asset izojoin; //Изостык
  bool izojoinreverse; //Указатель, что изостык развёрнут
  Asset[] tmpassets;

  float displacement;
  float vert_displ;


  final string GetIcon(string Name)
  {
    KUID kuid = me.GetAsset().FindAsset("extension-core").FindAsset(Name).GetKUID();
    return HTMLWindow.MakeImage(kuid.GetHTMLString(), true);
  }

  void UpdateIzojoin(void)
  {
    MeshObject mo = me.GetFXAttachment("izojoin");
    if(izojoin and (!mo or mo.GetAsset() != izojoin))
      mo = me.SetFXAttachment("izojoin", me.izojoin);
    else if(!me.izojoin) mo = me.SetFXAttachment("izojoin", null);
    if(mo and izojoinreverse)
      mo.SetMeshOrientation("default", 0.0, 0.0, Math.PI);
    else if(mo) mo.SetMeshOrientation("default", 0.0, 0.0, 0.0);
  }

  void UpdateSignalPosition(void)
  {
    me.SetMeshTranslation("default", displacement, 0, vert_displ);
  }

  final string GetAttachedSignalName()
  {
    if(RouterO.OwnerSignal) return RouterO.OwnerSignal.privateName + "@" + RouterO.OwnerSignal.stationName + " (" + RouterO.OwnerSignal.GetName() + ")";
    return "";
  }

  final string GetAttachedSignalDirection()
  {
    if(RouterO.ctrlDir) return strTable.GetString("linksignaldirectionforward");
    return strTable.GetString("linksignaldirectionbackward");
  }

  final string GetStringBool(bool stat)
  {
    if(stat) return strTable.GetString("yes");
    return strTable.GetString("no");
  }


  public string GetPropertyType(string id)
  {
	  if(id == "displace" or id == "vert_displ") return "float,-10,10,0.05";
    if(id == "izojoin") return "list,1";
    return inherited(id);
  }

  public string[] GetPropertyElementList(string propertyID)
  {
    if(propertyID == "izojoin"){
      Asset[] sassets = TrainzScript.GetAssetList("scenary", "TSIZJ");
      Asset[] massets = TrainzScript.GetAssetList("mesh", "TSIZJ");
      string[] ret = new string[sassets.size() + massets.size()];
      tmpassets = new Asset[sassets.size() + massets.size()];
      int i;
      for(i = 0; i < sassets.size() + massets.size(); i++){
        if(i >= sassets.size()){
          ret[i] = massets[i - sassets.size()].GetLocalisedName();
          tmpassets[i] = massets[i - sassets.size()];
        }else{
          ret[i] = sassets[i].GetLocalisedName();
          tmpassets[i]  = sassets[i];
        }
      }
      return ret;
    }
    return inherited(propertyID);
  }

  string GetPropertyName(string propertyID)
  {
    if(propertyID == "private-name") return strTable.GetString("interface-setroutename");
    else if(propertyID == "displace") return strTable.GetString("interface-offsetaxis");
    else if(propertyID == "vert_displ") return strTable.GetString("interface-offsetheight");
    else if(propertyID == "izojoin") return strTable.GetString("interface-izojoin");
    else if(propertyID == "twait") return strTable.GetString("interface-pointerdelay");
    return inherited(propertyID);
  }


  public string GetPropertyValue(string propertyID)
  {
    if(propertyID == "private-name") return privateName;
    else if(propertyID == "displace") return (string)displacement;
    else if(propertyID == "displace") return (string)displacement;
    else if(propertyID == "vert_displ") return (string)vert_displ;
    else if(propertyID == "twait") return (string)RouterO.timeToWait;
    return inherited(propertyID);
  }



  void SetPropertyValue(string id, float val)
  {
	  if(id == "displace"){
		  displacement = val;
      me.UpdateSignalPosition();
	  }else if(id == "vert_displ"){
		  vert_displ = val;
      me.UpdateSignalPosition();
    }else inherited(id, val);
  }

  void SetPropertyValue(string propertyID, string value, int index)
  {
    if(propertyID == "izojoin"){
      izojoin = tmpassets[index];
      izojoinreverse = false;
      me.UpdateIzojoin();
    }
    else inherited(propertyID, value, index);
  }

  public void LinkPropertyValue(string id)
  {
    if(id == "unlink_to"){
      if(RouterO.OwnerSignal) RouterO.OwnerSignal.SetLinkedMU(null);
      RouterO.OwnerSignal = null;
      signal_name = null;
      RouterO.UpdateMU();
    }else if(id == "deleteizojoin"){
      izojoin = null;
      me.UpdateIzojoin();
    }else if(id == "izojoinreverse"){
      izojoinreverse = !izojoinreverse;
      me.UpdateIzojoin();
    }else if(id[0,9]=="displace1"){
 			displacement = Str.ToFloat(id[10,]);
      me.UpdateSignalPosition();
    }
    else if(id == "offsetHeightlow") me.SetPropertyValue("vert_displ", -0.2);
    else if(id == "offsetHeightdef") me.SetPropertyValue("vert_displ", 0.0);
    else inherited(id);
  }



	public Soup GetProperties(void)
	{
		Soup soup = inherited();
  	soup.SetNamedTag("displacement", displacement);
	  soup.SetNamedTag("vert_displ", vert_displ);
    if(izojoin) soup.SetNamedTag("Izojoin", izojoin.GetKUID());
    if(izojoin) soup.SetNamedTag("IzojoinReverse", izojoinreverse);
    return soup;
  }

  public void SetProperties(Soup soup)
	{
	  inherited(soup);

    RouterO.SetProperties(soup);

	  privateName = soup.GetNamedTag("privateName");
	  me.ShowName(false);

	  signal_name = soup.GetNamedTag("Signal");
	  if(signal_name != "") RouterO.OwnerSignal = cast<zxSignal>Router.GetGameObject(signal_name);
    else  RouterO.OwnerSignal = null;

    KUID izjkuid = soup.GetNamedTagAsKUID("Izojoin");
    if(izjkuid) izojoin = World.FindAsset(izjkuid);
    else izojoin = null;
		izojoinreverse = soup.GetNamedTagAsBool("IzojoinReverse");
    me.UpdateIzojoin();

    displacement = soup.GetNamedTagAsFloat("displacement", me.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedTagAsFloat("default-position", 3.2));
    vert_displ = soup.GetNamedTagAsFloat("vert_displ", 0);
    me.UpdateSignalPosition();




    if(!IsInited) me.SetSoup();
    IsInited = true;
  }

  public void AppendDependencies(KUIDList io_dependencies)
  {
    inherited(io_dependencies);
    if(izojoin) io_dependencies.AddKUID(izojoin.GetKUID());
  }

  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    Soup soup = myAsset.GetConfigSoup().GetNamedSoup("extensions");
    isMacht = soup.GetNamedTagAsBool("ismacht");
    strTable = myAsset.FindAsset("extension-core").GetStringTable();
    ST = myAsset.FindAsset("main_lib").GetStringTable();
  }


};