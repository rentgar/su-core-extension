include "alsn_circuitrepeater.gs"
include "interfaceprovider.gs"
include "zx_signal.gs"
include "junction.gs"
include "trainzscript.gs"
include "Logic.gs"

//Описывает набор стрелок к светофору
class ZX_TrackCircuitSignalPath
{
  public int[] jncIndex; //Масив индексов стрелок к светофору
  public int[] jncDirection; //Масив состояния леверов стрелок
};


//Описывает привязку к светофору
class ZX_TrackCircuitSignal
{
  public zxSignal_main signal; //Светофор, к которому выполнена привязка
  public int signalId; //Идентификатор светофора к которому выполнена привязка
  public ZX_TrackCircuitSignalPath[] paths = new ZX_TrackCircuitSignalPath[0]; //Маршрут до светофора
  public int freq = ALSN_Provider.FREQ_NONE; //Частота кодирования сигнала
  public bool code_dev = false; //Указатель кодирования съездов
};



class ZX_TrackCircuit isclass ALSN_CircuitRepeater, InterfaceProviderBase //VBCF_SignalFreqChangerBase, InterfaceProviderBase
{
  Junction[] jncs; //Набор стерлок, которые участвуют в рельсовых цепях
  ZX_TrackCircuitSignal[] links; //Набор привязок светофоров
  ZX_TrackCircuitSignal link; //Текущая ссылка на привязку
  int[] jncsId; //Идентификаторы стрелок

  float offsetAxis; //Смещение знака от оси пути
  float offsetHeight; //Смещения знака по высоте
  float startOffsetAxis; //Положение от оси пути устанавливаемое изначально
  float defaultOffsetAxis; //Смещение от оси пути по умолчанию
  bool canReverse; //Возможность разворота изостыка по оси Z на 180 градусов
  bool canMove; //Возможность смещения знака от оси пути
  bool canCustomSign; //Возможность выбора стороннего знака
  string meshalsn25, meshalsn50, meshalsn75; //Набор мешей для информационного знака
  Asset customSign; //Указатель стороннего знака
  bool reverse; //Указатель, что объект развернут
  int currSign; //Текущий выбранный столб АЛСН

  bool calcState = false; //Указатель наличия расчёта
  string calcErrorMessage; //Тест сообщения об ошибке
  Asset[] tmpassets;


  //Указатель, что повторитель инициализирован и работает
  public bool Enabled(void){return (link and true);}

  //Получение светофора, для которого выполняется повторение
  public Signal ControlSignal(void)
  {
    if(link)return cast<Signal>(link.signal);
    return null;
  }

  //Получение провайдера, для которого выполняется повторение
  public ALSN_Provider ControlProvider(void)
  {
    if(link){
      object objSign = (object)link.signal;
      if(objSign)return cast<ALSN_Provider>objSign;
    }
    return null;
  }

  //Получение частоты кодирования сигнала.
  public int GetALSNFrequency(void)
  {
    if(link) return link.freq;
    return FREQ_NONE;
  }

  //Получение указателя кодирования съекздов.
  public int GetALSNSiding(void)
  {
    if(link and link.code_dev) return SIDING_TO;
    return SIDING_NONE;
  }

  //Получение кода АЛСН
  public int GetALSNCode(void)
  {
    ALSN_Provider provider = me.ControlProvider();
    if(provider) return provider.GetALSNCode();
    return CODE_NONE;
  }

  //Получение литеры светофора
  public string GetALSNSignalName()
  {
    ALSN_Provider provider = me.ControlProvider();
    if(provider) return provider.GetALSNSignalName();
    return "";
  }

  //Получение имени станции, на которой расположен светофор
  public string GetALSNStationName()
  {
    ALSN_Provider provider = me.ControlProvider();
    if(provider) return provider.GetALSNStationName();
    return "";
  }

  final void UpdateState(bool izojoin, bool sign, bool changeSign)
  {
    if(izojoin and reverse) me.SetMeshOrientation("default", 0.0, 0.0, Math.PI);
    else if(izojoin) me.SetMeshOrientation("default", 0.0, 0.0, 0.0);

    if(sign and me.HasMesh("signal-move"))
      me.SetMeshTranslation("signal-move", offsetAxis, 0, offsetHeight);

    if(changeSign){
      if(meshalsn25 != "" and me.HasMesh(meshalsn25)) me.SetMeshVisible(meshalsn25, currSign == 1, 0.0);
      if(meshalsn50 != "" and me.HasMesh(meshalsn50)) me.SetMeshVisible(meshalsn50, currSign == 2, 0.0);
      if(meshalsn75 != "" and me.HasMesh(meshalsn75)) me.SetMeshVisible(meshalsn75, currSign == 3, 0.0);
      if(currSign == 0 and customSign) me.SetFXAttachment("custom-signal", customSign);
      else me.SetFXAttachment("custom-signal", null);
    }
  }

  final void UpdateMarkers(void)
  {
    MeshObject infomrk = me.GetFXAttachment("infomarker");
    if(infomrk)infomrk.SetMeshVisible("exclamation", !jncs or !links or links.size() == 0, 0.0);
  }

  final void AddJunctionHandler(void)
  {
    int i;
    for(i = 0; i < jncs.size(); i++)
      me.AddHandler(me, "Junction", "Toggled", "JunctionHandler");
  }

  final thread void UpdateCurrentLink(void)
  {
    if(!jncs or !links or links.size() == 0){link = null;return;}
    int i, j, k;
    for(i = 0; i < links.size(); i++){
      if(links[i].paths.size() > 0){
        for(j = 0; j < links[i].paths.size(); j++){
          bool apply = links[i].paths[j].jncIndex.size() == 0;
          for(k = 0; k < links[i].paths[j].jncIndex.size(); k++){
            apply = jncs[links[i].paths[j].jncIndex[k]].GetDirection() == links[i].paths[j].jncDirection[k];
            if(!apply) break;
          }
          if(apply){link = links[i];return;}
        }
      }else{link = links[i];return;}
      me.Sleep(0.01);
    }
    link = null;
  }

  //Поиск следующего объекта маршрута
  final MapObject FindNextPathObject(GSTrackSearch GS)
  {
    MapObject MO = GS.SearchNext();
    float distance = GS.GetDistance();
    while(MO and !MO.isclass(Junction) and !(MO.isclass(zxSignal_main) and GS.GetFacingRelativeToSearchDirection() and
    (cast<zxSignal_main>MO).Type & (zxSignal.ST_IN | zxSignal.ST_OUT | zxSignal.ST_ROUTER | zxSignal.ST_PERMOPENED) and
    (cast<zxSignal_main>MO).Type & zxSignal.ST_UNTYPED and !((cast<zxSignal_main>MO).Type & zxSignal.ST_UNLINKED))){
      if(distance >= 5000.0 and MO.isclass(Trackside))GS = (cast<Trackside>MO).BeginTrackSearch(GS.GetFacingRelativeToSearchDirection()); //Если пройдено 5 км, то начинаем поиск от последнего объекта (защита от потери поиска)
      MO = GS.SearchNext();
      distance = GS.GetDistance();
    }
    return MO;
  }

  //Поиск следующего объекта для определения пошёртности стрелки
  final MapObject FindNextObject(GSTrackSearch GS, bool findMe)
  {
    MapObject MO = GS.SearchNext();
    float distance = GS.GetDistance();
    while(MO and !MO.isclass(Junction) and (!findMe or MO != me)){
      if(distance >= 5000.0 and MO.isclass(Trackside))GS = (cast<Trackside>MO).BeginTrackSearch(GS.GetFacingRelativeToSearchDirection()); //Если пройдено 5 км, то начинаем поиск от последнего объекта (защита от потери поиска)
      MO = GS.SearchNext();
      distance = GS.GetDistance();
    }
    return MO;
  }

  //Определение пошёрстности стрелки
  final int CheckJunctionBackDir(Junction cJunc, MapObject checker)
  {
    GSTrackSearch GS;
    MapObject MO;
    int i, j, ret = 0, src = cJunc.GetDirection();
    for(i = 0; i < 2; i++){
      for(j = 0; j < 2; j++){
        GS = cJunc.BeginTrackSearch((bool)i);
        MO = me.FindNextObject(GS, checker == me);
        if(cJunc.GetDirection() == JunctionBase.DIRECTION_LEFT){
          if(MO == checker) ret = Logic.SetStat(ret, 1, true);
          cJunc.SetDirection(JunctionBase.DIRECTION_RIGHT);
        }else if(cJunc.GetDirection() == JunctionBase.DIRECTION_RIGHT){
          if(MO == checker) ret = Logic.SetStat(ret, 2, true);
          cJunc.SetDirection(JunctionBase.DIRECTION_LEFT);
        }
      }
      if(ret > 0 and i == 0){
        ret = Logic.SetStat(ret, 4, true);
        break;
      }
    }
    return ret;
  }

  final thread void Calc(void)
  {
    ZX_TrackCircuitSignal[] crcs = new ZX_TrackCircuitSignal[0];
    Junction[] juncs = new Junction[0];
    bool[] jncsUsed = new bool[0];
    int[] pjncs = new int[0]; //Список пройденных стрелок
    int[] sDirection = new int[0]; //Коды sDirection: 1 - левое направление; 2 - правое направление; 4 - Стрелка протившерстная (иначе пошёрстная); 8 - левер по ходу движения
    int[] cDirection = new int[0]; //Направления для стрелки
    int[] jDirection = new int[0]; //Исходное направления для стрелки
    GSTrackSearch GS = me.BeginTrackSearch(true);
    MapObject MO;
    bool start = true;
    int slepper = 0, i, j, k;

    calcErrorMessage = "";
    calcState = true;
    jncs = null;
    links = null;
    link = null;

    while(start or pjncs.size() > 0){
      MO = me.FindNextPathObject(GS); //Выполняем поиск следующего объекта маршрута

      if(MO and MO.isclass(zxSignal_main)){ //Если встретился светофор
        ZX_TrackCircuitSignalPath path = new ZX_TrackCircuitSignalPath();
        path.jncIndex.copy(pjncs);
        path.jncDirection.copy(cDirection);
        ZX_TrackCircuitSignal crc = null;
        for(i = 0; i < crcs.size(); i++){
          if(crcs[i].signal == MO){
            crc = crcs[i];
            break;
          }
        }
        if(!crc){
          crc = new ZX_TrackCircuitSignal();
          crc.signal = cast<zxSignal_main>MO;
          crc.signalId = crc.signal.GetId();
          crc.code_dev = crc.signal.code_dev & 1;
          crc.freq = crc.signal.code_freq;
          crcs[crcs.size()] = crc;
        }
        crc.paths[crc.paths.size()] = path;
        if(pjncs.size() > 0) jncsUsed[pjncs[pjncs.size() - 1]] = true;
      }
      if(MO and MO.isclass(Junction)){ //Если объект является стрелкой, то
        int jIndex = -1, prevjncIndex = -1;
        for(i = 0; i < juncs.size(); i++){
          if(juncs[i] == MO){
            jIndex = i;
            break;
          }
        }
        if(jIndex == -1){
          juncs[juncs.size()] = cast<Junction>MO;
          jIndex = juncs.size() - 1;
          jncsUsed[jncsUsed.size()] = false;
        }
        int prevdircode, prevdir;
        MapObject checker = me;
        if(pjncs.size() > 0){
          prevjncIndex = pjncs[pjncs.size() - 1];
          prevdircode = sDirection[sDirection.size() - 1];
          prevdir = cDirection[cDirection.size() - 1];
          checker = juncs[prevjncIndex];
        }
        int rcode = me.CheckJunctionBackDir(juncs[jIndex], checker); //Проверяем пошёрстность стрелки
        if(rcode == 0){ //Если левер с ошибкой, то выдаём сообщения и покидаем расчёт
          calcErrorMessage = strTable.GetString1("error-lever_wrong_place", juncs[jIndex].GetName());
          calcState = false;
          return;
        }
        bool lface = rcode == 7 or rcode == 1 or rcode == 2; //Определяем направление левера на стрелке
        int cdir, dircode = Logic.SetStat(Logic.SetStat(0, 8, lface), 4, Logic.GetStat(rcode, 3));
        if(!Logic.GetStat(rcode, 3) and Logic.GetStat(rcode, 1)){ //Если стрелка пошёрстная c отклонение влево
          cdir = JunctionBase.DIRECTION_LEFT;
          dircode = Logic.SetStat(dircode, 1, true);
        }else if(!Logic.GetStat(rcode, 3) and Logic.GetStat(rcode, 2)){ //Если стрелка пошёрстная c отклонение вправо
          cdir = JunctionBase.DIRECTION_RIGHT;
          dircode = Logic.SetStat(dircode, 2, true);
        }else{
          cdir = juncs[jIndex].GetDirection();
          dircode = Logic.SetStat(dircode, 1, cdir == JunctionBase.DIRECTION_LEFT);
          dircode = Logic.SetStat(dircode, 2, cdir == JunctionBase.DIRECTION_RIGHT);
        }
        if(!Logic.GetStat(dircode, 4)) //Если срелка пошёрстная, то
          GS = juncs[jIndex].BeginTrackSearch(!Logic.GetStat(dircode, 8));//Начинаем от неё поиск

        pjncs[pjncs.size()] = jIndex;
        sDirection[sDirection.size()] = dircode;
        cDirection[cDirection.size()] = cdir;
        jDirection[jDirection.size()] = juncs[jIndex].GetDirection();
      }else{ //Если объект не является стрелкой и есть предыдущие стрелки
        bool br = false;
        while(pjncs.size() > 0 and !br){
          int ldircode = sDirection[sDirection.size() - 1];
          if(Logic.GetStat(ldircode, 4) and !Logic.GetStat(ldircode, 3)){ //Если стрелка противошёрстная и не все направления проверены
            if(Logic.GetStat(ldircode, 1)){
              cDirection[cDirection.size() - 1] = JunctionBase.DIRECTION_RIGHT;
              sDirection[sDirection.size() - 1] = Logic.SetStat(ldircode, 2, true);
            }else if(Logic.GetStat(ldircode, 2)){
              cDirection[cDirection.size() - 1] = JunctionBase.DIRECTION_LEFT;
              sDirection[sDirection.size() - 1] = Logic.SetStat(ldircode, 1, true);
            }
            juncs[pjncs[pjncs.size() - 1]].SetDirection(cDirection[cDirection.size() - 1]);
            GS = juncs[pjncs[pjncs.size() - 1]].BeginTrackSearch(Logic.GetStat(ldircode, 8));//Начинаем от неё поиск
            br = true;
          }else{
            int jncIndex = pjncs[pjncs.size() - 1];
            if(pjncs.size() > 1 and jncsUsed[jncIndex]) jncsUsed[pjncs[pjncs.size() - 2]] = true;
            juncs[jncIndex].SetDirection(jDirection[jDirection.size() - 1]);
            pjncs[pjncs.size() - 1, pjncs.size()] = null;
            sDirection[sDirection.size() - 1, sDirection.size()] = null;
            cDirection[cDirection.size() - 1, cDirection.size()] = null;
            jDirection[jDirection.size() - 1, jDirection.size()] = null;
          }
        }
      }
      start = false;
      if(slepper >= 100){Sleep(0.01); slepper = 0;} else slepper++;
    }
    if(crcs.size() > 0){
      Junction[] ujuncs = new Junction[0];
      int[] indexes = new int[juncs.size()];
      jncsId = new int[0];
      for(i = 0; i < juncs.size(); i++){
        if(jncsUsed[i]){
          indexes[i] = ujuncs.size();
          ujuncs[ujuncs.size()] = juncs[i];
          jncsId[i] = juncs[i].GetId();
        }
        if(slepper >= 100){Sleep(0.01); slepper = 0;} else slepper++;
      }
      for(i = 0; i < crcs.size(); i++){
        for(j = 0; j < crcs[i].paths.size(); j++){
          for(k = 0; k < crcs[i].paths[j].jncIndex.size(); k++)
            crcs[i].paths[j].jncIndex[k] = indexes[crcs[i].paths[j].jncIndex[k]];
          if(slepper >= 100){Sleep(0.01); slepper = 0;} else slepper++;
        }
      }
      jncs = ujuncs;
      links = crcs;
    }else calcErrorMessage = strTable.GetString("message-notfound");
    calcState = false;
  }

  final string GetStringFreq(int freq)
  {
    string ret;
    if(freq & FREQ_ALSN25) ret = ret + strTable.GetString("als25");
    else if(freq & FREQ_ALSN50) ret = ret + strTable.GetString("als50");
    else if(freq & FREQ_ALSN75) ret = ret + strTable.GetString("als75");
    if(freq & FREQ_ALSNEN and ret != "") return ret + " + " + strTable.GetString("als-en");
    else if(freq & FREQ_ALSNEN) return strTable.GetString("als-en");
    else if(ret == "") ret = strTable.GetString("als-none");
    return ret;
  }

  final string GetStringBool(bool stat)
  {
    if(stat) return strTable.GetString("yes");
    return strTable.GetString("no");
  }

  final string GetStringSelectedSignal(void)
  {
    if(currSign == 3) return strTable.GetString("als75");
    else if(currSign == 2) return strTable.GetString("als50");
    else if(currSign == 1) return strTable.GetString("als25");
    else if(currSign == 0) return strTable.GetString("customsignal");
    return strTable.GetString("no");
  }

//  final string GetCleanFloatString(float val)
//  {
//    float valk = ((float)((int)(Math.Fabs(val) * 100.0 + 0.5))) / 100;
//    if(val < 0) valk = -valk;
//    string sval = HTMLWindow.GetCleanFloatString(valk);
//    string[] args = Str.Tokens(sval, ".");
//    int currCount = 0;
//    if(args.size() == 2) currCount = args[1].size();
//    if(args.size() == 2 and currCount < 2) return args[0] + "." + args[1] + HTMLWindow.PadZerosOnFront("", 2 - currCount);
//    if(currCount < 2) return args[0] + "." + HTMLWindow.PadZerosOnFront("", 2 - currCount);
//    return sval;
//  }

	public string GetPropertyType(string id)
	{
    if(id == "calculate" or id == "update" or  id[0, 4] == "freq" or  id[0, 5] == "alsen" or  id[0, 7] == "codedev" or id == "reverse" or
    id == "setoffsetaxisleft" or id == "setoffsetaxisright" or id == "setoffsetheightlow" or id == "setoffsetheightdef") return "link";
		else if(id == "setoffsetaxis" or id == "setoffsetheight") return "float,-100.0,100.0,0.1";
    else if(id == "selectsign") return "list,0";
    else if(id == "selectcustomsign") return "list,1";
		return inherited(id);
	}

  public string[] GetPropertyElementList(string id)
  {
    if(id == "selectsign"){
      string[] ret = new string[1];
      ret[0] = strTable.GetString("no");
      if(canCustomSign)ret[ret.size()] = strTable.GetString("customsignal");
      if(meshalsn25 != "" and me.HasMesh(meshalsn25))ret[ret.size()] = strTable.GetString("als25");
      if(meshalsn50 != "" and me.HasMesh(meshalsn50))ret[ret.size()] = strTable.GetString("als50");
      if(meshalsn75 != "" and me.HasMesh(meshalsn75))ret[ret.size()] = strTable.GetString("als75");
      return ret;
    }else if(id == "selectcustomsign"){
      Asset[] sassets = TrainzScript.GetAssetList("scenery", "TSTCS");
      Asset[] massets = TrainzScript.GetAssetList("mesh", "TSTCS");
      string[] ret = new string[sassets.size() + massets.size()];
      tmpassets = new Asset[sassets.size() + massets.size()];
      int i;
      for(i = 0; i < sassets.size() + massets.size(); i++){
        if(i >= sassets.size()){
          ret[i] = massets[i - sassets.size()].GetLocalisedName();
          tmpassets[i] = massets[i - sassets.size()];
        }else{
          ret[i] = sassets[i].GetLocalisedName();
          tmpassets[i]  = sassets[i];
        }
      }
      return ret;
    }
    return inherited(id);
  }

  string GetPropertyName(string id)
  {
    if(id == "setoffsetaxis") return strTable.GetString("interface-offsetaxis");
    else if(id == "setoffsetheight") return strTable.GetString("interface-offsetheight");
    else if(id == "selectsign") return strTable.GetString("interface-signalitem");
    else if(id == "selectcustomsign") return strTable.GetString("interface-customsign");
    return inherited(id);
  }

  public string GetDescriptionHTML(void)
  {
    string ret = "<font size='10' color='#558ED5'><b>" + strTable.GetString("title-rc") + "</b></font><br><br>";
    tmpassets = null;


    ret = ret + HTMLWindow.StartTable("border='0' width='400'");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-calc"), "", CELL_HEAD) + HTMLWindow.EndRow();
    if(!calcState and calcErrorMessage != "") ret = ret + HTMLWindow.StartRow() + me.MakeCell(calcErrorMessage, "align=center", CELL_ERROR) + HTMLWindow.EndRow();
    else if(!calcState and jncs and links) ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("message-recalc"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    else if(!calcState) ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("message-notcalc"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    else ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("message-calc"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    if(!calcState) ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("calculate", strTable.GetString("calculate"), strTable.GetString("tooltip-calculate"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    else ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("update", strTable.GetString("update"), strTable.GetString("tooltip-update"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.EndTable() + "<br />";

    //Настройкеи внешнего вида
    if(canReverse or (me.HasMesh("signal-move") and (canCustomSign or meshalsn25 != "" or meshalsn50 != "" or meshalsn75 != ""))){
      ret = ret + HTMLWindow.StartTable("border='0' width='400'");
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-outside"), "colspan=4", CELL_HEAD) + HTMLWindow.EndRow();
      if(canReverse){
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("reverse-izojoin") + ":", "width='220'", CELL_KEY) +
                    me.MakeLinkCell("reverse", me.GetStringBool(reverse), strTable.GetString("tooltip-reverse-izojoin"), "colspan=3", CELL_VALUE) + HTMLWindow.EndRow();
      }
      if((me.HasMesh("signal-move") and (canCustomSign or meshalsn25 != "" or meshalsn50 != "" or meshalsn75 != ""))){
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("signalitem") + ":", "width='220'", CELL_KEY) +
                    me.MakeLinkCell("selectsign", me.GetStringSelectedSignal(), strTable.GetString("tooltip-signalitem"), "colspan=3", CELL_VALUE) + HTMLWindow.EndRow();
        if(currSign == 0 and canCustomSign){
          string sname = strTable.GetString("customsignalnot");
          int celltype = CELL_ERROR;
          if(customSign){sname = customSign.GetLocalisedName(); celltype = CELL_VALUE;}
          ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("customsignal") + ":", "width='220'", CELL_KEY) +
                      me.MakeLinkCell("selectcustomsign", sname, strTable.GetString("tooltip-customsignal"), "colspan=3", celltype) + HTMLWindow.EndRow();
        }
        if(currSign >= 0 and canMove){
          //ret = ret + HTMLWindow.EndTable() + HTMLWindow.StartTable("border='0' width='400'");
          ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("offsetaxis-sign") + ":", "width='220'", CELL_KEY) +
                      me.MakeLinkCell("setoffsetaxis", me.GetCleanFloatString(offsetAxis), strTable.GetString("tooltip-offsetaxis"), "width='60'", CELL_VALUE) +
                      me.MakeLinkCell("setoffsetaxisleft", me.GetCleanFloatString(-defaultOffsetAxis), strTable.GetString("tooltip-offsetaxis-defleft"), "width='60'", CELL_KEY) +
                      me.MakeLinkCell("setoffsetaxisright", me.GetCleanFloatString(defaultOffsetAxis), strTable.GetString("tooltip-offsetaxis-defright"), "width='60'", CELL_KEY) + HTMLWindow.EndRow();
          ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("offsetheight-sign") + ":", "width='220'", CELL_KEY) +
                      me.MakeLinkCell("setoffsetheight", me.GetCleanFloatString(offsetHeight), strTable.GetString("tooltip-offsetheight"), "width='60'", CELL_VALUE) +
                      me.MakeLinkCell("setoffsetheightlow", "-0.20", strTable.GetString("tooltip-offsetheightlow"), "width='60'", CELL_KEY) +
                      me.MakeLinkCell("setoffsetheightdef", "0.00", strTable.GetString("tooltip-offsetheightdef"), "width='60'", CELL_KEY) + HTMLWindow.EndRow();
        }
      }
      ret = ret + HTMLWindow.EndTable() + "<br />";
    }

/*
  if(canReverse or (me.HasMesh("signal-move") and (canCustomSign or meshalsn25 != "" or meshalsn50 != "" or meshalsn75 != ""))){
    float offsetAxis; //Смещение знака от оси пути
  float offsetHeight; //Смещения знака по высоте
  float startOffsetAxis; //Положение от оси пути устанавливаемое изначально
  float defaultOffsetAxis; //Смещение от оси пути по умолчанию
  bool canReverse; //Возможность разворота изостыка по оси Z на 180 градусов
  bool canMove; //Возможность смещения знака от оси пути
  bool canCustomSign; //Возможность выбора стороннего знака
  string meshalsn25, meshalsn50, meshalsn75; //Набор мешей для информационного знака
  MeshObject customSign; //Указатель стороннего знака
  bool reverse; //Указатель, что объект развернут
  int currSign; //Текущий выбранный столб АЛСН

  */




    if(jncs and links){
      int i, j, k;
      for(i = 0; i < links.size(); i++){
        string name = links[i].signal.privateName;
        if(!(links[i].signal.Type & zxSignal.ST_PERMOPENED)) name = name + "@" + links[i].signal.stationName;
        ret = ret + HTMLWindow.StartTable("border='0' width='400'");
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-cercuit"), "colspan ='2'", CELL_HEAD) + HTMLWindow.EndRow();
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("signal") + ":", "width=140", CELL_KEY) +
                    me.MakeCell(name + " (" + links[i].signal.GetName() + ")", "", CELL_KEY_DISABLED) + HTMLWindow.EndRow();
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("freq") + ":", "width=140", CELL_KEY) +
                    me.MakeCell(me.GetStringFreq(links[i].signal.code_freq), "", CELL_KEY_DISABLED) + HTMLWindow.EndRow();
        if(links[i].signal.Type & (zxSignal.ST_OUT | zxSignal.ST_ROUTER))
          ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("codedev") + ":", "width=140", CELL_KEY) +
          me.MakeCell(me.GetStringBool(links[i].signal.code_dev & 1), "", CELL_KEY_DISABLED) + HTMLWindow.EndRow();
        ret = ret + HTMLWindow.StartRow() + me.MakeCell("", "colspan ='2'", CELL_KEY) + HTMLWindow.EndRow();

        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("freq") + ":", "width=140", CELL_KEY) +
                    me.MakeLinkCell("freq/" + i, me.GetStringFreq(links[i].freq & 7), strTable.GetString("tooltip-freq"), "", CELL_VALUE) + HTMLWindow.EndRow();
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("als-en") + ":", "width=140", CELL_KEY) +
                    me.MakeLinkCell("alsen/" + i, me.GetStringBool(links[i].freq & FREQ_ALSNEN), strTable.GetString("tooltip-alsen"), "", CELL_VALUE) + HTMLWindow.EndRow();
        if(links[i].signal.Type & (zxSignal.ST_OUT | zxSignal.ST_ROUTER))
          ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("codedev") + ":", "width=140", CELL_KEY) +
                      me.MakeLinkCell("codedev/" + i, me.GetStringBool(links[i].code_dev), strTable.GetString("tooltip-codedev"), "", CELL_VALUE) + HTMLWindow.EndRow();
        ret = ret + HTMLWindow.EndTable() + "<br />";
      }
    }
    return ret;
  }

  public void LinkPropertyValue(string id){
    if(id == "calculate"){
      calcState = true;
      me.Calc();
    }else if(id[0, 4] == "freq"){
      int index = Str.ToInt(id[5,]);
      links[index].freq = Logic.SetStat(links[index].freq, 16, links[index].freq & (FREQ_ALSN25 | FREQ_ALSN50 | FREQ_ALSN75));
      links[index].freq = Logic.SetStat(links[index].freq, FREQ_ALSN75, links[index].freq & FREQ_ALSN50);
      links[index].freq = Logic.SetStat(links[index].freq, FREQ_ALSN50, links[index].freq & FREQ_ALSN25);
      links[index].freq = Logic.SetStat(links[index].freq, FREQ_ALSN25, !(links[index].freq & (FREQ_ALSN25 | FREQ_ALSN50 | FREQ_ALSN75 | 16)));
      links[index].freq = Logic.SetStat(links[index].freq, 16, false);
    }else if(id[0, 5] == "alsen"){
      int index = Str.ToInt(id[6,]);
      links[index].freq = Logic.SetStat(links[index].freq, FREQ_ALSNEN, !(links[index].freq & FREQ_ALSNEN));
    }else if(id[0, 7] == "codedev"){
      int index = Str.ToInt(id[8,]);
      links[index].code_dev = !links[index].code_dev;
    }else if(id == "reverse"){
      reverse = canReverse and !reverse;
      me.UpdateState(true, false, false);
    }else if(id == "setoffsetaxisleft"){
      offsetAxis = -defaultOffsetAxis;
      me.UpdateState(false, true, currSign == 0);
    }else if(id == "setoffsetaxisright"){
      offsetAxis = defaultOffsetAxis;
      me.UpdateState(false, true, currSign == 0);
    }else if(id == "setoffsetheightlow"){
      offsetHeight = -0.2;
      me.UpdateState(false, true, currSign == 0);
    }else if(id == "setoffsetheightdef"){
      offsetHeight = 0.0;
      me.UpdateState(false, true, currSign == 0);
    }else inherited(id);
  }

  void SetPropertyValue(string id, float value)
  {
    if(id == "setoffsetaxis"){
      offsetAxis = value;
      me.UpdateState(false, true, currSign == 0);
    }else if(id == "setoffsetheight"){
      offsetHeight = value;
      me.UpdateState(false, true, currSign == 0);
    }else inherited(id, value);
  }


  void SetPropertyValue(string id, string value, int index)
  {
    if(id == "selectsign"){
      if(!canCustomSign and index >= 1) index = index + 1;
      if((meshalsn25 == "" or !me.HasMesh(meshalsn25)) and index >= 2) index = index + 1;
      if((meshalsn50 == "" or !me.HasMesh(meshalsn50)) and index >= 3) index = index + 1;
      if((meshalsn75 == "" or !me.HasMesh(meshalsn75)) and index >= 4) index = index + 1;
      currSign = index - 1;
      me.UpdateState(false, false, true);
    }else if(id == "selectcustomsign"){
      customSign = tmpassets[index];
      me.UpdateState(false, false, true);
    }else inherited(id, value, index);
  }


  thread void tmpsub(void)
  {
    while(true){
      if(link) me.SetFXNameText("desc", link.signal.privateName + "@" + link.signal.stationName);
      else me.SetFXNameText("desc", "--NONE--");
      me.Sleep(1.0);
    }
  }

  final void JunctionHandler(Message msg)
  {
    me.UpdateCurrentLink();
  }

  void ModuleInitHandler(Message msg) {
    if(World.GetCurrentModule() == World.DRIVER_MODULE and jncs and links){
      me.AddJunctionHandler();
      me.UpdateCurrentLink();
      me.tmpsub();
    }
  }

   public Soup GetProperties(void)
  {
    Soup soup = inherited(), tmpsoup = Constructors.NewSoup();
    soup.SetNamedTag("Reverse", reverse);
    soup.SetNamedTag("CurrentSignal", currSign);
    soup.SetNamedTag("OffsetAxis", offsetAxis);
    soup.SetNamedTag("OffsetHeight", offsetHeight);
    if(customSign and currSign == 0)soup.SetNamedTag("CustomSignal", customSign.GetKUID());
    if(jncs and links){
      tmpsoup.Copy(soup);
      int i, j, k;
      soup.SetNamedTag("JunctionCount", jncs.size());
      for(i = 0; i < jncs.size(); i++){
        if(!jncs[i] or jncs[i] != Router.GetGameObject(jncsId[i])){
          jncs = null; links = null; link = null; jncsId = null;
          //me.UpdateMarkers();
          return tmpsoup;
        }
        soup.SetNamedTag("JunctionName." + i, jncs[i].GetName());
      }

      soup.SetNamedTag("CircuitCount", links.size());
      for(i = 0; i < links.size(); i++){
        if(!links[i].signal or links[i].signal != Router.GetGameObject(links[i].signalId) or
        !(links[i].signal.Type & (zxSignal.ST_IN | zxSignal.ST_OUT | zxSignal.ST_ROUTER | zxSignal.ST_PERMOPENED))){
          jncs = null; links = null; link = null; jncsId = null;
          //me.UpdateMarkers();
          return tmpsoup;
        }
        soup.SetNamedTag("CircuitSignal." + i, links[i].signal.GetName());
        soup.SetNamedTag("CircuitFreq." + i, links[i].freq);
        soup.SetNamedTag("CircuitCodeDev." + i, links[i].code_dev);
        soup.SetNamedTag("CircuitPathCount." + i, links[i].paths.size());
        for(j = 0; j < links[i].paths.size(); j++){
          soup.SetNamedTag("CircuitPathJunctionCount." + i + "." + j, links[i].paths[j].jncIndex.size());
          for(k = 0; k < links[i].paths[j].jncIndex.size(); k++){
            soup.SetNamedTag("CircuitPathJunctionIndex." + i + "." + j + "." + k, links[i].paths[j].jncIndex[k]);
            soup.SetNamedTag("CircuitPathJunctionDirection." + i + "." + j + "." + k, links[i].paths[j].jncDirection[k]);
          }
        }
      }
    }
    return soup;
  }

	public void SetProperties(Soup soup)
  {
    inherited(soup);
    int i, j, k;
    int jcount = soup.GetNamedTagAsInt("JunctionCount", -1);
    int ccount = soup.GetNamedTagAsInt("CircuitCount", -1);
    calcErrorMessage = ""; jncs = null; links = null; link = null;

    reverse = soup.GetNamedTagAsBool("Reverse");
    currSign = soup.GetNamedTagAsInt("CurrentSignal", -1);
    offsetAxis = soup.GetNamedTagAsFloat("OffsetAxis", startOffsetAxis);
    offsetHeight = soup.GetNamedTagAsFloat("OffsetHeight");
    KUID cskuid = soup.GetNamedTagAsKUID("CustomSignal");
    if(cskuid and currSign == 0) customSign = World.FindAsset(cskuid);
    else customSign = null;
    me.UpdateState(true, true, true);

    if(jcount >= 0 and ccount > 0){
      jncs = new Junction[jcount];
      jncsId = new int[jcount];
      for(i = 0;  i < jcount; i++){
        jncs[i] = cast<Junction>Router.GetGameObject(soup.GetNamedTag("JunctionName." + i));
        if(!jncs[i]){
          jncs = null; links = null; link = null; jncsId = null;
          me.UpdateMarkers();
          return;
        }
        jncsId[i] = jncs[i].GetId();
      }

      links = new ZX_TrackCircuitSignal[ccount];
      for(i = 0; i < ccount; i++){
        zxSignal_main sign = cast<zxSignal_main>Router.GetGameObject(soup.GetNamedTag("CircuitSignal." + i));
        if(!sign){
          jncs = null; links = null; link = null; jncsId = null;
          me.UpdateMarkers();
          return;
        }
        int pcount = soup.GetNamedTagAsInt("CircuitPathCount." + i);
        ZX_TrackCircuitSignal lnk = new ZX_TrackCircuitSignal();
        lnk.signal = sign;
        lnk.signalId = sign.GetId();
        lnk.freq = soup.GetNamedTagAsInt("CircuitFreq." + i);
        lnk.code_dev = soup.GetNamedTagAsBool("CircuitCodeDev." + i);
        lnk.paths = new ZX_TrackCircuitSignalPath[pcount];
        for(j = 0; j < pcount; j++){
          int jcount = soup.GetNamedTagAsInt("CircuitPathJunctionCount." + i + "." + j);
          ZX_TrackCircuitSignalPath path = new ZX_TrackCircuitSignalPath();
          path.jncIndex = new int[jcount];
          path.jncDirection = new int[jcount];
          for(k = 0; k < jcount; k++){
            path.jncIndex[k] = soup.GetNamedTagAsInt("CircuitPathJunctionIndex." + i + "." + j + "." + k);
            path.jncDirection[k] = soup.GetNamedTagAsInt("CircuitPathJunctionDirection." + i + "." + j + "." + k);
          }
          lnk.paths[j] = path;
        }
        links[i] = lnk;
      }
    }
    me.UpdateMarkers();
  }

  public void AppendDependencies(KUIDList io_dependencies)
  {
    inherited(io_dependencies);
    if(customSign and currSign == 0)io_dependencies.AddKUID(customSign.GetKUID());
  }

  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    Asset libAsset = myAsset.FindAsset("extension-core-library");
    strTable = libAsset.GetStringTable();
    me.AddHandler(me, "World", "ModuleInit", "ModuleInitHandler");

    Soup config = myAsset.GetConfigSoup().GetNamedSoup("extensions");
    canReverse = config.GetNamedTagAsBool("canreverse");
    canCustomSign = config.GetNamedTagAsBool("cancustomsignal");
    canMove = config.GetNamedTagAsBool("canmove");
    startOffsetAxis = config.GetNamedTagAsFloat("startoffset", 3.1);
    defaultOffsetAxis = Math.Fabs(config.GetNamedTagAsFloat("defaultoffset", 3.1));
    meshalsn25 = config.GetNamedTag("mesh-als25");
    meshalsn50 = config.GetNamedTag("mesh-als50");
    meshalsn75 = config.GetNamedTag("mesh-als75");
  }

};






















